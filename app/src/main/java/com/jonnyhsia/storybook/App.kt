package com.jonnyhsia.storybook

import android.app.Application
import android.graphics.Typeface
import android.support.v4.content.res.ResourcesCompat
import android.support.v7.app.AppCompatDelegate
import com.jonnyhsia.storybook.utils.Preference
import kotlin.properties.Delegates

/**
 * Created by JonnyHsia on 17/8/3.
 * Application
 */
class App : Application() {

    override fun onCreate() {
        super.onCreate()
        INSTANCE = this

        val isNightMode by Preference(this, Preference.NIGHT_MODE, false)
        AppCompatDelegate.setDefaultNightMode(
                if (isNightMode) AppCompatDelegate.MODE_NIGHT_YES else AppCompatDelegate.MODE_NIGHT_NO)
    }

    companion object {
        // 使用 NotNull 委托
        var INSTANCE: App by Delegates.notNull()
            private set

        val TYPEFACE: Typeface? by lazy {
            ResourcesCompat.getFont(INSTANCE, R.font.noto_sans)
        }
    }

}