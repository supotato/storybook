package com.jonnyhsia.storybook

import android.content.Context

import com.bumptech.glide.Glide
import com.bumptech.glide.GlideBuilder
import com.bumptech.glide.Registry
import com.bumptech.glide.annotation.GlideModule
import com.bumptech.glide.load.engine.cache.LruResourceCache
import com.bumptech.glide.module.AppGlideModule

/**
 * Created by JonnyHsia on 17/8/8.
 */
@GlideModule
class MyGlideModule : AppGlideModule() {

    override fun applyOptions(context: Context?, builder: GlideBuilder?) {
        super.applyOptions(context, builder)
        builder?.setMemoryCache(LruResourceCache(20 * 1024 * 1024))
    }

    override fun registerComponents(context: Context?, glide: Glide?, registry: Registry?) {
        super.registerComponents(context, glide, registry)
        /*registry.append(String.class, InputStream.class, GlideUrlLoader.factory);*/
    }

    override fun isManifestParsingEnabled(): Boolean {
        return false
    }
}
