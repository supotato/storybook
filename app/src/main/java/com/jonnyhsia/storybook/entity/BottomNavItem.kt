package com.jonnyhsia.storybook.entity

/**
 * Created by JonnyHsia on 17/8/2.
 * 底部导航 POJO
 */
data class BottomNavItem(var title: String?,
                         var res: Int,
                         var isPrimary: Boolean = false)