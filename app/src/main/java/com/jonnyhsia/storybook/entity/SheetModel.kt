package com.jonnyhsia.storybook.entity

/**
 * Created by JonnyHsia on 17/9/15.
 */
data class SheetModel(var title: String,
                      var content: String,
                      var image: String,
                      var action: String)