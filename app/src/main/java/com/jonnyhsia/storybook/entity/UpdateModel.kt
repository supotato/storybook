package com.jonnyhsia.storybook.entity;

data class UpdateModel(var latestVersion : String,
                       var downloadUrl : String,
                       var image : String? = null,
                       var meta : String? = null)