package com.jonnyhsia.storybook.exception

/**
 * Created by JonnyHsia on 17/8/31.
 * App 异常基类
 */
open class AppException(msg: String) : Exception(msg)