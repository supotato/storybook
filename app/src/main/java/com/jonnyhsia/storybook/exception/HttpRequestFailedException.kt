package com.jonnyhsia.storybook.exception

/**
 * Created by JonnyHsia on 17/8/31.
 * HTTP 请求失败异常
 */
class HttpRequestFailedException(msg: String? = null)
    : AppException(msg ?: "未知原因请求服务器失败")