package com.jonnyhsia.storybook.exception

/**
 * Created by JonnyHsia on 17/8/31.
 * 网络无连接
 */
class NetworkNotWorkingException(msg: String = "网络无连接") : AppException(msg)