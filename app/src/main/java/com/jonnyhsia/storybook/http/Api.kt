package com.jonnyhsia.storybook.http

import com.jonnyhsia.storybook.entity.SheetModel
import com.jonnyhsia.storybook.entity.UpdateModel
import com.jonnyhsia.storybook.persistence.Story
import com.jonnyhsia.storybook.entity.User
import io.reactivex.Flowable
import io.reactivex.Observable
import io.reactivex.Single
import retrofit2.http.Field
import retrofit2.http.FieldMap
import retrofit2.http.FormUrlEncoded
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Path
import retrofit2.http.Query

/**
 * Created by JonnyHsia on 17/7/24.
 * API 接口
 */
interface Api {

    companion object {
        val BASE_URL = "http://139.224.238.88:8080/story/"
        val TEST_URL = "http://169.254.227.83:8080/story/"
    }

    @POST("user/register")
    @FormUrlEncoded
    fun register(@FieldMap user: Map<String, String>)
            : Observable<Response<User>>

    @POST("user/login")
    @FormUrlEncoded
    fun login(@Field("username") username: String,
              @Field("password") password: String)
            : Observable<Response<User>>

    @POST("story/publish")
    @FormUrlEncoded
    fun publishStory(@FieldMap storyMap: Map<String, String>)
            : Single<Response<Long>>

    @GET("story/{username}/timeline")
    fun getTimelineStory(@Path("username") username: String,
                         @Query("offset") offset: Int = 0,
                         @Query("limit") limit: Int = 20)
            : Single<Response<ArrayList<Story>>>

    @GET("story/{story_id}")
    fun getStory(@Path("story_id") storyId: Long)
            : Flowable<Response<Story>>

    @POST("story/{story_id}/delete")
    fun deleteStory(@Path("story_id") storyId: Long)
            : Single<Response<Int>>

    @GET("app/check_for_updates")
    fun checkForUpdates(@Query("versionCode") versionCode: Int,
                        @Query("versionName") versionName: String)
            : Single<Response<UpdateModel>>
}