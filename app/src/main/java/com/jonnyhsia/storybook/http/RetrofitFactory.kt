package com.jonnyhsia.storybook.http

import android.util.Log
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

/**
 * Created by JonnyHsia on 17/5/26.
 * Retrofit 提供器
 */
class RetrofitFactory {

    companion object {
        fun build(baseUrl: String): Retrofit {
            val client = OkHttpClient.Builder()
                    .addInterceptor(HttpLoggingInterceptor(HttpLoggingInterceptor.Logger {
                        Log.d("HttpLog", it)
                    }).setLevel(HttpLoggingInterceptor.Level.BODY))
                    .readTimeout(8, TimeUnit.SECONDS)
                    .writeTimeout(8, TimeUnit.SECONDS)
                    .build()

            return Retrofit.Builder()
                    .baseUrl(baseUrl)
                    .client(client)
                    .addConverterFactory(GsonConverterFactory.create())
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .build()
        }
    }

}