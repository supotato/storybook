package com.jonnyhsia.storybook.persistence

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey

/**
 * Created by JonnyHsia on 17/9/12.
 * Banner 实体类
 */
@Entity(tableName = "story")
data class Banner(@PrimaryKey
                  @ColumnInfo(name = "content_url") var contentUrl: String,
                  var title: String,
                  var msg: String,
                  var imgs: Array<String>) {

}
