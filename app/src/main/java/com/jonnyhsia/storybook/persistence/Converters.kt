package com.jonnyhsia.storybook.persistence

import android.arch.persistence.room.TypeConverter

import java.util.Date

/**
 * TypeConverters for Room
 */
class Converters {

    @TypeConverter
    fun fromTimestamp(value: Long): Date {
        return Date(value)
    }

    @TypeConverter
    fun dateToTimestamp(date: Date): Long {
        return date.time
    }
}
 