package com.jonnyhsia.storybook.persistence

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import java.util.Date

/**
 * Created by JonnyHsia on 17/8/29.
 * Story 实体类
 * 注意: 构造方法不可以有默认值参数
 */
@Entity(tableName = "story")
data class Story(@PrimaryKey
                 @ColumnInfo(name = "story_id")
                 var storyId: Long,
                 var title: String,
                 var content: String,
                 var author: String,
                 var images: String,
                 @ColumnInfo(name = "create_time") var createTime: Date) {

    override fun equals(other: Any?): Boolean {
        return if (other is Story) {
            storyId == other.storyId
                    && title == other.title
                    && content == other.content
                    && images == other.images
        } else {
            super.equals(other)
        }
    }

    override fun hashCode(): Int {
        var result = storyId.hashCode()
        result = 31 * result + title.hashCode()
        result = 31 * result + content.hashCode()
        result = 31 * result + images.hashCode()
        return result
    }

    /**
     * 将字符串转换成 [List]
     */
    fun imagesArray(): ArrayList<String> {
        return images.split(delimiters = ";") as ArrayList
    }

    /**
     * 将传入的 [List] 转换成字符串
     * @param imgs 图片的 URL 数据
     */
    fun setImageArray(imgs: List<String>) {
        val builder = StringBuilder()
        for (i in 0 until imgs.size) {
            builder.append(imgs[i])
            if (i == imgs.size - 1) {
                builder.append(";")
            }
        }
        images = builder.toString()
    }

    companion object {
        fun generateSimpleStory(id: Long) = Story(id, "", "", "", "", Date())
    }
}
