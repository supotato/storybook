package com.jonnyhsia.storybook.persistence

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Delete
import android.arch.persistence.room.Insert
import android.arch.persistence.room.OnConflictStrategy
import android.arch.persistence.room.Query
import io.reactivex.Flowable

/**
 * Created by JonnyHsia on 17/8/30.
 * Data Access Object for the Story table.
 */
@Dao
interface StoryDao {

    @Query("SELECT * FROM Story WHERE story_id = :id LIMIT 1")
    fun queryStoryById(id: String): Flowable<Story>

    @Query("SELECT * FROM Story ORDER BY story_id DESC")
    fun queryAllStory(): Flowable<List<Story>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertStory(story: Story)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertStories(stories: List<Story>)

    @Query("DELETE FROM Story")
    fun clearStories()

    @Delete()
    fun deleteStory(story: Story)

}