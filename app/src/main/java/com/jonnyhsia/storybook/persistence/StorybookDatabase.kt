package com.jonnyhsia.storybook.persistence

import android.arch.persistence.room.Database
import android.arch.persistence.room.Room
import android.arch.persistence.room.RoomDatabase
import android.arch.persistence.room.TypeConverters
import android.content.Context

/**
 * Created by JonnyHsia on 17/8/30.
 * 故事书数据库
 */
@Database(entities = arrayOf(Story::class), version = 1)
@TypeConverters(Converters::class)
abstract class StorybookDatabase : RoomDatabase() {

    abstract fun storyDao(): StoryDao

    companion object {
        /**
         * StorybookDatabase 单例
         */
        @Volatile private var INSTANCE: StorybookDatabase? = null

        /**
         * 获取 StorybookDatabase 的实例
         * @param context 用于构造 Database 的上下文
         */
        fun instance(context: Context): StorybookDatabase =
                INSTANCE ?: synchronized(StorybookDatabase::class.java) {
                    INSTANCE ?: buildInstance(context).also { INSTANCE = it }
                }

        /**
         * 构造 StorybookDatabase 实例
         */
        private fun buildInstance(context: Context): StorybookDatabase =
                Room.databaseBuilder(context.applicationContext,
                        StorybookDatabase::class.java, "Database.db")
                        .build()
    }
}