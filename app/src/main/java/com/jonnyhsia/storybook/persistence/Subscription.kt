package com.jonnyhsia.storybook.persistence

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey

/**
 * Created by JonnyHsia on 17/9/12.
 * Banner 实体类
 */
@Entity(tableName = "subscription")
data class Subscription(@PrimaryKey
                        @ColumnInfo(name = "id") var id: Long,
                        var title: String,
                        var description: String,
                        var img: String) {

}