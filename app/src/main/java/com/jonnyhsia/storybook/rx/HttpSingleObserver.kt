package com.jonnyhsia.storybook.rx

import com.jonnyhsia.storybook.exception.HttpRequestFailedException
import com.jonnyhsia.storybook.http.Response
import com.jonnyhsia.storybook.utils.loge

import io.reactivex.SingleObserver
import io.reactivex.disposables.Disposable

/**
 * Created by JonnyHsia on 17/8/29.
 * 封装 HTTP 请求基本的 [SingleObserver]
 */
class HttpSingleObserver<T>(private val onResponse: ((responseData: T?) -> Unit),
                            private val onException: ((message: String?) -> Unit)? = null)
    : SingleObserver<Response<T>> {

    override fun onSubscribe(d: Disposable) {

    }

    /**
     * 请求成功
     */
    override fun onSuccess(response: Response<T>) {
        if (response.success) {
            onResponse.invoke(response.data)
        } else {
            onError(HttpRequestFailedException(response.error))
        }
    }

    /**
     * 异常处理
     */
    override fun onError(e: Throwable) {
        if (onException == null) {
            loge(e.message, e)
        } else {
            // 使用外部自定义的异常解决方案
            onException.invoke(e.message)
        }
    }
}