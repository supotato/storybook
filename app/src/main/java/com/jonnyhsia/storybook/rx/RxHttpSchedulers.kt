package com.jonnyhsia.storybook.rx

import com.jonnyhsia.storybook.exception.NetworkNotWorkingException
import com.jonnyhsia.storybook.utils.Utils
import io.reactivex.ObservableTransformer
import io.reactivex.SingleTransformer
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

/**
 * Created by JonnyHsia on 17/8/29.
 * 有关调度的封装
 */
class RxHttpSchedulers {
    companion object {
        /**
         * @return ObservableTransformer
         */
        fun <T> composeObservable(): ObservableTransformer<T, T> {
            return ObservableTransformer {
                it.subscribeOn(Schedulers.io())
                        .unsubscribeOn(Schedulers.io())
                        .doOnSubscribe {
                            // 网络连接判断
                            // 若无网络连接则取消
                            if (!Utils.isNetworkWorking() && !it.isDisposed) {
                                it.dispose()
                                throw  NetworkNotWorkingException()
                            }
                        }
                        .observeOn(AndroidSchedulers.mainThread())
            }
        }

        /**
         * @return SingleTransformer
         */
        fun <T> composeSingle(): SingleTransformer<T, T> {
            return SingleTransformer {
                it.subscribeOn(Schedulers.io())
                        .doOnSubscribe {
                            // 网络连接判断
                            // 若无网络连接则取消
                            if (!Utils.isNetworkWorking() && !it.isDisposed) {
                                throw  NetworkNotWorkingException()
                                // it.dispose()
                            }
                        }
                        .observeOn(AndroidSchedulers.mainThread())
            }
        }
    }
}