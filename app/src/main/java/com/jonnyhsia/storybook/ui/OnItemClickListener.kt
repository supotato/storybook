package com.jonnyhsia.storybook.ui

import android.support.v7.widget.RecyclerView
import android.view.GestureDetector
import android.view.MotionEvent
import com.jonnyhsia.storybook.utils.logd

/**
 * Created by JonnyHsia on 17/8/2.
 * [RecyclerView] 的手势监听
 */
class OnItemClickListener(var recyclerView: RecyclerView,
                          var onClick: (id: Int, holder: RecyclerView.ViewHolder) -> Unit,
                          var onDoubleClick: ((id: Int, holder: RecyclerView.ViewHolder) -> Unit)? = null)
    : RecyclerView.OnItemTouchListener {

    private var mGestureDetector: GestureDetector? = null

    init {
        mGestureDetector = GestureDetector(recyclerView.context, ItemGestureListener())
    }

    // 解析坐标点和触摸规律来识别触摸手势
    override fun onTouchEvent(rv: RecyclerView?, e: MotionEvent?) {
        mGestureDetector?.onTouchEvent(e)
    }

    // 解析坐标点和触摸规律来识别触摸手势
    override fun onInterceptTouchEvent(rv: RecyclerView?, e: MotionEvent?): Boolean {
        mGestureDetector?.onTouchEvent(e)
        return false
    }

    // 处理触摸冲突
    override fun onRequestDisallowInterceptTouchEvent(disallowIntercept: Boolean) {

    }

    inner class ItemGestureListener : GestureDetector.SimpleOnGestureListener() {
        // 普通的点击事件
        override fun onSingleTapUp(e: MotionEvent?): Boolean {
            if (e == null) {
                logd("MotionEvent is null.")
                return false
            }
            val item = recyclerView.findChildViewUnder(e.x, e.y)
            if (item != null) {
                onClick.invoke(item.id, recyclerView.getChildViewHolder(item))
            }
            // 判断 item 是否是 ViewGroup
            // 如果是, 则遍历每一项
            /*if (item is ViewGroup) {
                for (i in 0..item.childCount - 1) {
                    item.getChildAt(i).setOnClickListener {
                        onClick.invoke(it.id, recyclerView.getChildViewHolder(item))
                    }
                }
            } else if (item is View) {
                onClick.invoke(item.id, recyclerView.getChildViewHolder(item))
            }*/
            return true
        }

        // 双击事件
        override fun onDoubleTap(e: MotionEvent?): Boolean {
            if (e == null) {
                logd("MotionEvent is null")
                return false
            }
            val child = recyclerView.findChildViewUnder(e.x, e.y)
            if (child != null) {
                val holder = recyclerView.getChildViewHolder(child)
                onDoubleClick?.invoke(child.id, holder)
            }
            return true
        }

        // 长按事件
        override fun onLongPress(e: MotionEvent?) {
            if (e == null) {
                return
            }
            val child = recyclerView.findChildViewUnder(e.x, e.y)
        }
    }
}