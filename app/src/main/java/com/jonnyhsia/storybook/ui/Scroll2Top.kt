package com.jonnyhsia.storybook.ui

/**
 * Created by JonnyHsia on 17/8/3.
 * 滑动到顶部
 */
interface Scroll2Top {

    /**
     * 滑动到顶部
     */
    fun scroll2Top(): Unit

}