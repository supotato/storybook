package com.jonnyhsia.storybook.ui.activity

import android.os.Bundle
import android.support.v4.widget.NestedScrollView
import android.support.v7.widget.LinearLayoutManager
import android.view.Gravity
import android.view.MenuItem
import com.github.rubensousa.gravitysnaphelper.GravityPagerSnapHelper
import com.jonnyhsia.storybook.R
import com.jonnyhsia.storybook.entity.SheetModel
import com.jonnyhsia.storybook.http.Api
import com.jonnyhsia.storybook.http.RetrofitFactory
import com.jonnyhsia.storybook.rx.RxHttpSchedulers
import com.jonnyhsia.storybook.ui.adapter.ShowcaseAdapter
import com.jonnyhsia.storybook.ui.fragment.SPBottomSheetDialogFragment
import com.jonnyhsia.storybook.utils.Utils
import com.jonnyhsia.storybook.utils.jump2Activity
import com.jonnyhsia.storybook.utils.toast
import kotlinx.android.synthetic.main.activity_about.*

class AboutActivity : DayNightActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_about)

        initView()
    }

    private fun initView() {
        setSupportActionBar(toolbar)
        supportActionBar?.title = ""
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setHomeButtonEnabled(true)

        val changeLogs = resources.getStringArray(R.array.change_log).asList()
        val changeLogBuilder = StringBuilder()
        changeLogs.map { "- $it" }
                .forEachIndexed { index, change ->
                    changeLogBuilder.append(change)
                    if (index != changeLogs.size - 1) {
                        changeLogBuilder.append("\n")
                    }
                }
        tvChangeLog?.text = changeLogBuilder

        tvVersion?.text = getString(R.string.app_version, Utils.getVersionName(this))
        tvDate?.text = getString(R.string.version_date)
        tvCheckForUpdate?.setOnClickListener { checkForUpdates() }

        rvShowcase?.setHasFixedSize(true)
        rvShowcase?.layoutManager = LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false)
        rvShowcase?.adapter = ShowcaseAdapter(arrayListOf(R.mipmap.showcase_timeline, R.mipmap.showcase_discover, R.mipmap.showcase_timeline))
        GravityPagerSnapHelper(Gravity.START).attachToRecyclerView(rvShowcase)

        kvBuiltWith?.onClick = {
            jump2Activity(ThirdPartyLibrariesActivity::class.java)
        }

        kvDonate?.onClick = {
            if (Utils.getVersionName(this).contains("Beta")) {
                toast("等正式版发布再资瓷我吧~")
            } else {
                toast("还没做好接受捐助的准备啦~")
            }
        }
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            android.R.id.home -> {
                finish()
            }
        }
        return super.onOptionsItemSelected(item)
    }

    private fun checkForUpdates() {
        val api = RetrofitFactory.build(Api.BASE_URL).create(Api::class.java)
        val code = 1
        val name = Utils.getVersionName(this)
        api.checkForUpdates(code, name)
                .compose(RxHttpSchedulers.composeSingle())
                .subscribe({ response ->
                    if (response?.data == null) {
                        toast("已经是最新版惹")
                    }
                    response?.data?.let {
                        val sheetModel = SheetModel(title = "检查到应用更新",
                                content = "检查到日记书有版本更新, 快去找豆豆要安装包吧吸吸~\n版本号:${it.latestVersion}\n更新日志: 暂无。",
                                image = "",
                                action = "知道了")
                        val dialog = SPBottomSheetDialogFragment.newInstance(sheetModel)
                        dialog.show(supportFragmentManager, "UpdateDialog")
                    }
                }, { error ->
                    error.printStackTrace()
                })
    }
}
