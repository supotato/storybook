package com.jonnyhsia.storybook.ui.activity

import android.os.Bundle

import com.jonnyhsia.storybook.R
import com.jonnyhsia.storybook.utils.Preference
import kotlinx.android.synthetic.main.activity_account.tvLogout
import android.content.Intent

class AccountActivity : DayNightActivity() {
    var username by Preference(this, Preference.USERNAME, "", {
        updateUsername(it)
    })

    var avatar by Preference(this, Preference.AVATAR, "", {
        updateAvatar(it)
    })
    // var id by Preference(this, Preference.ID, -1L)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_account)

        initView()
    }

    private fun initView() {
        tvLogout?.setOnClickListener {
            username = ""

            // 清除安全验证
            var lock by Preference(this, Preference.LOCK, Preference.LOCK_NONE)
            if (lock != Preference.LOCK_NONE) {
                lock = Preference.LOCK_NONE
            }

            val intent = Intent(this, WelcomeActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
        }
    }

    private fun updateUsername(newValue: String): Unit {

    }

    private fun updateAvatar(newValue: String): Unit {

    }

}
