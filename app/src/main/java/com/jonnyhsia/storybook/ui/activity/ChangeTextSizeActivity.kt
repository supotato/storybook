package com.jonnyhsia.storybook.ui.activity

import android.animation.ObjectAnimator
import android.app.Activity
import android.os.Build
import android.os.Bundle
import android.util.SparseIntArray
import android.view.MenuItem
import android.view.View
import android.view.animation.AccelerateDecelerateInterpolator
import android.widget.SeekBar
import com.jonnyhsia.storybook.R
import com.jonnyhsia.storybook.utils.Preference
import com.jonnyhsia.storybook.utils.UIUtils
import com.jonnyhsia.storybook.utils.Utils
import kotlinx.android.synthetic.main.activity_change_text_size.seekBar
import kotlinx.android.synthetic.main.activity_change_text_size.text
import kotlinx.android.synthetic.main.activity_change_text_size.tvSize
import kotlinx.android.synthetic.main.toolbar.toolbar

class ChangeTextSizeActivity : DayNightActivity() {

    var textSize by Preference(this, Preference.STORY_TEXT_SIZE, 15)
    val sizeMap = SparseIntArray()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_change_text_size)

        setSupportActionBar(toolbar)
        supportActionBar?.title = getString(R.string.activity_custom_text_size)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setHomeButtonEnabled(true)

        sizeMap.put(0, 12)
        sizeMap.put(1, 13)
        sizeMap.put(2, 14)
        sizeMap.put(3, 15)
        sizeMap.put(4, 16)
        sizeMap.put(5, 17)
        sizeMap.put(6, 18)

        seekBar?.progress = sizeMap.indexOfValue(textSize)
        text?.textSize = textSize.toFloat()
        tvSize?.text = Utils.size2String(textSize, this@ChangeTextSizeActivity)

        seekBar?.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
            override fun onProgressChanged(p0: SeekBar?, progress: Int, p2: Boolean) {
                textSize = sizeMap[progress]
                tvSize?.text = Utils.size2String(textSize, this@ChangeTextSizeActivity)
                // TextView.textSize 返回的是 px 单位的大小, 需要转换
                val spSize = UIUtils.px2sp(this@ChangeTextSizeActivity, text.textSize)
                val anim = ObjectAnimator.ofFloat(text, "textSize", spSize, textSize.toFloat())
                anim.duration = 200
                anim.interpolator = AccelerateDecelerateInterpolator()
                anim.start()
            }

            override fun onStartTrackingTouch(p0: SeekBar?) = Unit

            override fun onStopTrackingTouch(p0: SeekBar?) = Unit
        })
    }

    fun smaller(view: View): Unit {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            seekBar?.setProgress(seekBar.progress - 1, true)
        } else {
            seekBar?.progress = seekBar.progress - 1
        }
    }

    fun bigger(view: View): Unit {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            seekBar?.setProgress(seekBar.progress + 1, true)
        } else {
            seekBar?.progress = seekBar.progress + 1
        }
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            android.R.id.home -> {
                setResult(Activity.RESULT_CANCELED)
                finish()
            }
        }
        return super.onOptionsItemSelected(item)
    }
}
