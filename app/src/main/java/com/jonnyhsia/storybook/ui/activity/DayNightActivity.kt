package com.jonnyhsia.storybook.ui.activity

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.jonnyhsia.storybook.utils.Preference
import com.jonnyhsia.storybook.utils.StatusBarColorUtils

/**
 * Created by JonnyHsia on 17/8/3.
 * 亮色状态栏活动
 */
open class DayNightActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val isNightMode by Preference(this, Preference.NIGHT_MODE, false)
        if (isNightMode) {
            StatusBarColorUtils.setStatusBarDarkIcon(this, false)
        } else {
            // 设置为亮色状态栏
            StatusBarColorUtils.setStatusBarDarkIcon(this, true)
        }
    }

}