package com.jonnyhsia.storybook.ui.activity

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import com.jonnyhsia.storybook.R
import com.jonnyhsia.storybook.utils.Preference
import com.jonnyhsia.storybook.utils.Preference.Companion.LOCK_PATTERN
import com.jonnyhsia.storybook.utils.Preference.Companion.LOCK_CODE
import kotlinx.android.synthetic.main.activity_god_mode.kvGodMode
import kotlinx.android.synthetic.main.activity_god_mode.kvLock
import kotlinx.android.synthetic.main.activity_god_mode.kvLockType

class GodModeActivity : DayNightActivity() {

    companion object {
        val DATA_GOD = "dataGod"
    }

    val data: Intent by lazy {
        Intent()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_god_mode)

        initView()
    }

    private var godMode by Preference(this, Preference.GOD_MODE, false)
    private var lockType by Preference(this, Preference.LOCK, Preference.LOCK_NONE)

    private fun initView() {

        kvGodMode?.isChecked = godMode
        kvGodMode?.onClick = {
            godMode = it == true
            data.putExtra(DATA_GOD, it)
        }

        when (lockType) {
            LOCK_PATTERN -> {
                kvLock.isChecked = true
                kvLockType.isChecked = true
            }
            LOCK_CODE -> {
                kvLock.isChecked = true
            }
        }

        kvLock.onClick = {
            if (it == false) {
                lockType = Preference.LOCK_NONE
            }
        }

        kvLockType?.onClick = {
            lockType = when {
                !kvLock.isChecked -> Preference.LOCK_NONE
                kvLockType.isChecked -> LOCK_PATTERN
                else -> LOCK_CODE
            }
        }
    }

    override fun onBackPressed() {
        setResult(Activity.RESULT_OK, data)
        super.onBackPressed()
    }

    override fun onDestroy() {
        super.onDestroy()
    }
}
