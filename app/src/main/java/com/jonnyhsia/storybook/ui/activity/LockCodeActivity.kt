package com.jonnyhsia.storybook.ui.activity

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.View
import com.jonnyhsia.storybook.R

class LockCodeActivity : DayNightActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_lock_code)
    }

    fun test(view: View): Unit {
        setResult(Activity.RESULT_OK)
        finish()
    }

    override fun onBackPressed() {
        super.onBackPressed()
        setResult(Activity.RESULT_CANCELED)
    }

    companion object {
        private val EXTRA_MODE = "extraMode"

        /**
         * @param verification 是否是验证模式
         */
        fun getLockIntent(context: Context, verification: Boolean): Intent {
            val intent = Intent(context, LockCodeActivity::class.java)
            intent.putExtra(EXTRA_MODE, verification)
            return intent
        }
    }
}
