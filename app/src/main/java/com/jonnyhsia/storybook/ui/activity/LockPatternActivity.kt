package com.jonnyhsia.storybook.ui.activity

import android.app.Activity
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import com.andrognito.patternlockview.PatternLockView
import com.andrognito.patternlockview.utils.PatternLockUtils
import com.andrognito.rxpatternlockview.RxPatternLockView
import com.jonnyhsia.storybook.GlideApp
import com.jonnyhsia.storybook.R
import com.jonnyhsia.storybook.ui.glide.CircleTransform
import com.jonnyhsia.storybook.utils.Preference
import kotlinx.android.synthetic.main.activity_lock_pattern.imgAvatar
import kotlinx.android.synthetic.main.activity_lock_pattern.patternView
import kotlinx.android.synthetic.main.activity_lock_pattern.tvNickname

class LockPatternActivity : DayNightActivity() {

    companion object {
        val TAG = "Pattern"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_lock_pattern)

        initView()
    }

    private var tryCount = 5

    private fun initView() {
        val username: String by Preference(this, Preference.NICKNAME, "高能的土豆")
        val avatar: String by Preference(this, Preference.AVATAR, "http://ou4f31a1x.bkt.clouddn.com/17-8-5/89394276.jpg")
        val pattern by Preference(this, Preference.PATTERN_STRING, "012")

        tvNickname.text = username
        GlideApp.with(this)
                .load(avatar)
                .transform(CircleTransform())
                .into(imgAvatar)

        patternView.isTactileFeedbackEnabled = false
        RxPatternLockView.patternComplete(patternView)
                .subscribe {
                    val patternString = PatternLockUtils.patternToString(patternView, it.pattern)
                    when {
                        patternString.length == 1 -> return@subscribe
                        patternString == pattern -> {
                            patternView?.setViewMode(PatternLockView.PatternViewMode.CORRECT)
                            verified()
                        }
                        else -> {
                            patternView?.setViewMode(PatternLockView.PatternViewMode.WRONG)
                            if (--tryCount == 0) {
                                finish()
                            } else {
                                Toast.makeText(this, "You have $tryCount more try :(", Toast.LENGTH_SHORT).show()
                            }
                        }
                    }

                    Log.d(TAG, "Complete")
                }
    }

    private fun verified() {
        setResult(Activity.RESULT_OK)
        finish()
    }

    override fun onBackPressed() {
        super.onBackPressed()
        setResult(Activity.RESULT_CANCELED)
    }

}
