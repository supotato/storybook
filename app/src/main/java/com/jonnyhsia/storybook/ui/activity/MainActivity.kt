package com.jonnyhsia.storybook.ui.activity

import android.content.Intent
import android.os.Bundle
import android.os.CountDownTimer
import android.support.v4.app.Fragment

import com.jonnyhsia.storybook.R
import com.jonnyhsia.storybook.entity.BottomNavItem
import com.jonnyhsia.storybook.entity.SheetModel
import com.jonnyhsia.storybook.http.Api
import com.jonnyhsia.storybook.http.RetrofitFactory
import com.jonnyhsia.storybook.rx.HttpSingleObserver
import com.jonnyhsia.storybook.rx.RxHttpSchedulers
import com.jonnyhsia.storybook.ui.Scroll2Top
import com.jonnyhsia.storybook.ui.fragment.DiscoverFragment
import com.jonnyhsia.storybook.ui.fragment.MeFragment
import com.jonnyhsia.storybook.ui.fragment.SPBottomSheetDialogFragment
import com.jonnyhsia.storybook.ui.fragment.TimelineFragment
import com.jonnyhsia.storybook.utils.Preference
import com.jonnyhsia.storybook.utils.Utils
import com.jonnyhsia.storybook.utils.jump2Activity
import com.jonnyhsia.storybook.utils.toast
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_main.bottomNavigation
import kotlin.properties.Delegates

class MainActivity : DayNightActivity() {

    companion object {
        val REQUEST_UNLOCK = 100
        val STATE_INDEX = "index"
        val STATE_VERIFY = "verify"
    }

    // (是否是)懒加载(?) Fragment
    private val timelineFragment: Fragment by lazy { TimelineFragment() }
    private val meFragment: Fragment by lazy { MeFragment() }
    private val discoverFragment: Fragment by lazy { DiscoverFragment() }
    private val overviewFragment: Fragment by lazy { Fragment() }

    // 显示的 Fragment 的索引
    private var indexState = 0

    val username by Preference(this, Preference.USERNAME, "")
    private var godMode by Preference(this, Preference.GOD_MODE, false, {
        (getFragmentByIndex(4) as? MeFragment)?.godModeVisibility(true)
        toast("God Mode: ON")
    })
    private val lockType by Preference(this, Preference.LOCK, Preference.LOCK_NONE)

    override fun onCreate(savedInstanceState: Bundle?) {
        // 切换回正常主题
        setTheme(R.style.AppTheme_NoTitle)
        super.onCreate(savedInstanceState)

        if (username.isEmpty()) {
            finish()
            jump2Activity(WelcomeActivity::class.java)
        }

        // 判断是否需要恢复数据
        if (savedInstanceState != null) {
            indexState = savedInstanceState.getInt(STATE_INDEX)
            verified = savedInstanceState.getBoolean(STATE_VERIFY)
        }

        // 验证
        checkVerify()

        setContentView(R.layout.activity_main)
        initView()
    }

    private fun initView() {
        supportFragmentManager.beginTransaction()
                .replace(R.id.container, getFragmentByIndex(indexState))
                .commit()
        val navItems = arrayListOf(
                BottomNavItem("故事", R.mipmap.ic_timeline),
                BottomNavItem("合辑", R.mipmap.ic_stories),
                BottomNavItem("光影", R.mipmap.ic_cal),
                BottomNavItem("我的", R.mipmap.ic_me))

        // 初始化 BottomNavigation
        bottomNavigation.setNavItems(navItems)
                .addPrimarySelectListener {
                    jump2Activity(WritingActivity::class.java)
                }
                .addItemSelectListener { old, pos, _ ->
                    navigateFragment(old, pos)
                }
                .addItemReselectListener { pos, _ ->
                    (getFragmentByIndex(pos) as? Scroll2Top)?.scroll2Top()
                    // God Mode
                    if (pos == 4 && clickTimes != -1) {
                        clickTimes++
                    }
                }
        bottomNavigation.activeIndex = indexState
    }

    var clickTimes: Int by Delegates.observable(0, { _, oldValue, newValue ->
        when (newValue) {
            1 -> countDown.start()
            5 -> {
                clickTimes = -1
                countDown.cancel()
                godMode = true
            }
        }
    })

    /**
     * 倒计时
     */
    private val countDown = object : CountDownTimer(3600, 3600) {
        override fun onFinish() {
            if (clickTimes < 5) clickTimes = 0
        }

        override fun onTick(p0: Long) {
        }
    }

    /**
     * 导航 Fragment
     */
    private fun navigateFragment(oldPos: Int, pos: Int) {
        if (oldPos == pos) {
            return
        }
        val oldFragment = getFragmentByIndex(oldPos)
        val newFragment = getFragmentByIndex(pos)

        val xaction = supportFragmentManager.beginTransaction()
        xaction.setCustomAnimations(R.anim.popup_enter, R.anim.popup_exit)
        xaction.hide(oldFragment)

        if (newFragment.isAdded) {
            if (newFragment.isHidden) {
                xaction.show(newFragment)
            }
        } else {
            xaction.add(R.id.container, newFragment)
        }

        xaction.commit()
    }

    /**
     * 获取 index 对应的 Fragment
     */
    private fun getFragmentByIndex(pos: Int): Fragment {
        return when (pos) {
            0 -> timelineFragment
            1 -> discoverFragment
            3 -> overviewFragment
            4 -> meFragment
            else -> throw Exception("Fragment index invalid!")
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when (requestCode) {
            REQUEST_UNLOCK -> {
                if (resultCode == RESULT_OK) {
                    verified = true
                } else {
                    finish()
                }
            }
        }
    }

    override fun onSaveInstanceState(outState: Bundle?) {
        // 设置主题后, 恢复原来的状态
        // 保存 BottomNavigation 的选中位置
        // 保存用户是否已通过验证
        outState?.putInt(STATE_INDEX, bottomNavigation.activeIndex)
        outState?.putBoolean(STATE_VERIFY, verified)
        super.onSaveInstanceState(outState)
    }

    /**
     * 安全验证 Flag
     */
    private var verified = false

    /**
     * 安全验证
     */
    private fun checkVerify() {
        if (!verified) {
            when (lockType) {
                Preference.LOCK_PATTERN -> {
                    jump2Activity(LockPatternActivity::class.java, REQUEST_UNLOCK)
                }
                Preference.LOCK_CODE -> {
                    startActivityForResult(LockCodeActivity.getLockIntent(this, true), REQUEST_UNLOCK)
                }
            }
        }
    }

    override fun onResume() {
        super.onResume()
        clickTimes = if (godMode) -1 else 0
    }
}