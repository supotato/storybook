package com.jonnyhsia.storybook.ui.activity

import android.content.Context
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.inputmethod.InputMethodManager
import com.jakewharton.rxbinding2.widget.RxTextView
import com.jonnyhsia.storybook.App
import com.jonnyhsia.storybook.R
import com.jonnyhsia.storybook.persistence.Story
import com.jonnyhsia.storybook.persistence.StorybookDatabase
import com.jonnyhsia.storybook.ui.multitype.ImageStoryViewBinder
import com.jonnyhsia.storybook.ui.multitype.TextStoryViewBinder
import com.jonnyhsia.storybook.utils.logd
import com.jonnyhsia.storybook.utils.loge
import com.jonnyhsia.storybook.utils.toast
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_search.rvResult
import kotlinx.android.synthetic.main.activity_search.searchBar
import kotlinx.android.synthetic.main.activity_search.tvCancel
import me.drakeet.multitype.Items
import me.drakeet.multitype.MultiTypeAdapter
import java.util.concurrent.TimeUnit

class SearchActivity : DayNightActivity() {

    private var rvAdapter: MultiTypeAdapter? = null
    private var items: Items = Items()
    private var stories = ArrayList<Story>()

    /**
     * 管理 Rx 事件的订阅 (?)
     */
    private val disposable = CompositeDisposable()
    private var imm: InputMethodManager? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_search)
        imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager

        tvCancel?.setOnClickListener {
            // 关闭软键盘

            imm?.hideSoftInputFromWindow(window.decorView.windowToken, 0)
            onBackPressed()
        }

        initRecyclerView()
        initSearchBar()
    }

    override fun onStart() {
        super.onStart()

        disposable.add(StorybookDatabase.instance(this).storyDao().queryAllStory()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    stories.addAll(it)
                }, {
                    loge(it.message, it)
                    toast(it.message)
                })
        )
    }

    private fun initSearchBar() {
        searchBar?.typeface = App.TYPEFACE
        searchBar?.requestFocus()
        searchBar?.showSoftInputOnFocus = true
        searchBar?.requestFocusFromTouch()
        imm?.showSoftInput(searchBar, InputMethodManager.SHOW_FORCED)

        RxTextView.textChanges(searchBar)
                .debounce(400, TimeUnit.MILLISECONDS, AndroidSchedulers.mainThread())
                .subscribeOn(AndroidSchedulers.mainThread())
                .filter { it.isNotEmpty() }
                .subscribe { keywords ->
                    logd(keywords.toString())
                    val searchResult = Items()
                    stories.filterTo(searchResult, { (_, title, content) ->
                        title.contains(keywords) || content.contains(keywords)
                    })
                    items = searchResult
                    rvAdapter?.items = items
                    rvAdapter?.notifyDataSetChanged()
                }
    }

    private fun initRecyclerView() {
        // Item 点击事件
        val onClick = { pos: Int ->
            val item = items[pos] as? Story
            if (item != null) {
                startActivity(StoryDetailActivity.getStoryIntent(this, item.storyId))
                finish()
            }
        }

        // RecyclerView Layout Manager
        rvResult?.layoutManager = LinearLayoutManager(this)

        // RecyclerView MultiType Adapter
        rvAdapter = MultiTypeAdapter(items)
        rvAdapter?.register(Story::class.java)?.to(
                TextStoryViewBinder(onClick),
                ImageStoryViewBinder(onClick)
        )?.withClassLinker {
            if (it.images.isEmpty()) {
                return@withClassLinker TextStoryViewBinder::class.java
            } else {
                return@withClassLinker ImageStoryViewBinder::class.java
            }
        }
        rvResult?.adapter = rvAdapter
    }

    override fun onDestroy() {
        disposable.clear()
        super.onDestroy()
    }
}
