package com.jonnyhsia.storybook.ui.activity

import android.app.Activity
import android.os.Bundle
import android.view.MenuItem
import com.andrognito.patternlockview.PatternLockView
import com.andrognito.patternlockview.PatternLockView.PatternViewMode.*
import com.andrognito.patternlockview.utils.PatternLockUtils
import com.andrognito.rxpatternlockview.RxPatternLockView
import com.andrognito.rxpatternlockview.events.PatternLockCompoundEvent.EventType.*

import com.jonnyhsia.storybook.R
import com.jonnyhsia.storybook.utils.Preference
import com.jonnyhsia.storybook.utils.toast
import kotlinx.android.synthetic.main.activity_set_pattern.patternView
import kotlinx.android.synthetic.main.activity_set_pattern.tvCaption
import kotlinx.android.synthetic.main.toolbar.toolbar

class SetPatternActivity : DayNightActivity() {

    private var lockType by Preference(this, Preference.LOCK, Preference.LOCK_NONE)
    private var pattern by Preference(this, Preference.PATTERN_STRING, "")
    private var settingPattern: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_set_pattern)

        initView()
    }

    private fun initView() {
        setSupportActionBar(toolbar)
        supportActionBar?.title = getString(R.string.activity_set_pattern)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setHomeButtonEnabled(true)

        // 文字提示与动画演示图形绘制
        tvCaption?.text = getString(R.string.start_draw_pattern)
        patternView?.setPattern(AUTO_DRAW, mutableListOf(
                PatternLockView.Dot.of(0, 2),
                PatternLockView.Dot.of(0, 1),
                PatternLockView.Dot.of(0, 0),
                PatternLockView.Dot.of(1, 0),
                PatternLockView.Dot.of(2, 0),
                PatternLockView.Dot.of(2, 1),
                PatternLockView.Dot.of(2, 2),
                PatternLockView.Dot.of(1, 2),
                PatternLockView.Dot.of(1, 1)))
        // 关闭振动反馈
        patternView?.isTactileFeedbackEnabled = false
        RxPatternLockView.patternChanges(patternView)
                .subscribe({
                    when (it?.eventType) {
                        PATTERN_PROGRESS -> {
                            tvCaption?.text = getString(R.string.drawing_pattern)
                        }
                        PATTERN_COMPLETE -> {
                            val patternString = PatternLockUtils.patternToString(patternView, it.pattern)
                            if (patternString.length >= 3) {
                                if (pattern.isEmpty()) {
                                    newPattern(patternString)
                                } else {
                                    changePattern(patternString)
                                }
                            } else {
                                patternView?.setViewMode(WRONG)
                                tvCaption?.text = getString(R.string.pattern_too_simple)
                            }
                        }
                    }
                })
    }

    private fun changePattern(patternString: String) {
        patternView?.setViewMode(CORRECT)
        pattern = patternString
        toast("完成, 请牢记解锁图案")
        finish()
    }

    private fun newPattern(patternString: String) {
        // 判断是否是第一次设置
        if (settingPattern == null) {
            patternView?.setViewMode(CORRECT)
            settingPattern = patternString
            tvCaption.text = "再次绘制图案以确认"
        } else {
            if (settingPattern == patternString) {
                patternView?.setViewMode(CORRECT)
                setResult(Activity.RESULT_OK)
                setPattern()
                toast("完成, 请牢记解锁图案")
                finish()
            } else {
                patternView?.setViewMode(WRONG)
                settingPattern = null
                tvCaption.text = getString(R.string.pattern_different)
            }
        }
    }

    private fun setPattern() {
        if (settingPattern != null) {
            lockType = Preference.LOCK_PATTERN
            pattern = settingPattern!!
        }
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {

        when (item?.itemId) {
            android.R.id.home -> {
                setResult(Activity.RESULT_CANCELED)
                finish()
            }
        }
        return super.onOptionsItemSelected(item)
    }
}
