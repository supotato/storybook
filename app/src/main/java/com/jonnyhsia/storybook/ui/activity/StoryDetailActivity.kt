package com.jonnyhsia.storybook.ui.activity

import android.animation.Animator
import android.animation.ObjectAnimator
import android.arch.lifecycle.ViewModelProviders
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.design.widget.FloatingActionButton
import android.support.v4.view.ViewPager
import android.view.View
import android.view.animation.AccelerateDecelerateInterpolator
import com.jonnyhsia.storybook.R
import com.jonnyhsia.storybook.http.Api
import com.jonnyhsia.storybook.http.RetrofitFactory
import com.jonnyhsia.storybook.persistence.Story
import com.jonnyhsia.storybook.persistence.StorybookDatabase
import com.jonnyhsia.storybook.rx.HttpSingleObserver
import com.jonnyhsia.storybook.rx.RxHttpSchedulers
import com.jonnyhsia.storybook.ui.adapter.StoryFragmentPagerAdapter
import com.jonnyhsia.storybook.ui.viewmodel.StoriesViewModel
import com.jonnyhsia.storybook.ui.viewmodel.ViewModelFactory
import com.jonnyhsia.storybook.utils.logd
import com.jonnyhsia.storybook.utils.loge
import com.jonnyhsia.storybook.utils.toast
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_story_detail.*
import kotlinx.android.synthetic.main.content_story_detail.viewPager
import kotlin.collections.ArrayList
import kotlin.properties.Delegates

class StoryDetailActivity : DayNightActivity() {

    /**
     * Toolbar 显隐 Flag
     */
    private var toolbarShownOrWillShow = false

    /**
     * ViewPager Adapter
     */
    private var pagerAdapter: StoryFragmentPagerAdapter? = null

    /**
     * Story View Model
     */
    private lateinit var viewModel: StoriesViewModel

    /**
     * View Model Factory
     */
    private lateinit var viewModelFactory: ViewModelFactory

    /**
     * 管理 Rx 事件的订阅 (?)
     */
    private val disposable = CompositeDisposable()

    /**
     * 故事数据
     */
    private var storyData by Delegates.observable(ArrayList<Story>(), { _, oldValue, newValue ->
        if (oldValue != newValue) {
            pagerAdapter?.updateStoryData(newValue)
            // 跳转到默认页
            val story = newValue.find { it.storyId == intent.getLongExtra(ID, -1L) }
            viewPager?.setCurrentItem(newValue.indexOf(story), false)
        }
    })

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        window.attributes.systemUiVisibility = View.SYSTEM_UI_FLAG_LOW_PROFILE
        setContentView(R.layout.activity_story_detail)
        if (intent == null) {
            finish()
        }

        // 初始化 ViewModel 与 Factory
        viewModelFactory = ViewModelFactory(StorybookDatabase.instance(this).storyDao())
        viewModel = ViewModelProviders.of(this, viewModelFactory).get(StoriesViewModel::class.java)

        // 初始化
        initClick()
        initView()
    }


    override fun onStart() {
        super.onStart()
        // 添加对本地故事列表数据的"观察"
        disposable.add(viewModel.stories()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ stories ->
                    // 成功查询到故事
                    storyData = stories as ArrayList<Story>
                    disposable.clear()
                }, { e ->
                    loge(e.message, e)
                }))
    }


    private fun initView() {
        pagerAdapter = StoryFragmentPagerAdapter(supportFragmentManager, storyData)
        viewPager.apply {
            systemUiVisibility = View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
            adapter = pagerAdapter
            pageMargin = resources.getDimensionPixelOffset(R.dimen.pager_margin)
            setPageMarginDrawable(R.drawable.pager_drawable)
            addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
                override fun onPageScrollStateChanged(state: Int) {
                    if (state == ViewPager.SCROLL_STATE_DRAGGING) {
                        fab?.show(fabListener)
                        hideToolbar()
                    }
                }

                override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {}

                override fun onPageSelected(position: Int) {}
            })
        }
    }

    /**
     * 初始化 Toolbar 的点击事件
     */
    private fun initClick() {
        fab?.setOnClickListener {
            fab.hide(fabListener)
        }
        btnClose?.setOnClickListener {
            finish()
        }
        btnEdit?.setOnClickListener {
            pagerAdapter?.adapterData(viewPager.currentItem)?.let {
                val intent = WritingActivity.getWritingIntent(this, it)
                startActivityForResult(intent, REQUEST_EDIT)
            }
        }
        btnDelete?.setOnClickListener {
            val story = pagerAdapter?.adapterData(viewPager.currentItem) ?: return@setOnClickListener
            requestDeleteStory(story)
        }
        btnNothing?.setOnClickListener {
            hideToolbar()
        }
    }

    /**
     * 请求删除故事
     */
    private fun requestDeleteStory(story: Story) {
        // TODO 显示进度条
        val api = RetrofitFactory.build(Api.BASE_URL).create(Api::class.java)
        api.deleteStory(story.storyId)
                .compose(RxHttpSchedulers.composeSingle())
                .subscribe(HttpSingleObserver({ data ->
                    updateDbAndUi(story)
                }, {
                    loge(it)
                }))
    }

    /**
     * 更新数据库与 UI
     */
    private fun updateDbAndUi(data: Story) {
        viewModel.deleteStory(data)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    if (pagerAdapter?.count ?: 0 <= 1) {
                        finish()
                        return@subscribe
                    }
                    val pos = minOf(viewPager.currentItem, pagerAdapter!!.count - 1)
                    pagerAdapter?.deleteStory(data)
                    viewPager.currentItem = pos
                    hideToolbar()
                }, {
                    loge(it.message)
                })
    }

    /**
     * Activity 回调
     */
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when (requestCode) {
            REQUEST_EDIT -> {
                if (resultCode == RESULT_OK && data != null) {
                    val title = data.getStringExtra(WritingActivity.TITLE) ?: ""
                    val content = data.getStringExtra(WritingActivity.CONTENT) ?: ""
                    val imgs = data.getStringExtra(WritingActivity.IMGS) ?: ""
                    pagerAdapter?.updateOneStory(viewPager.currentItem, title, content, imgs)
                }
            }
        }
    }

    /**
     * 隐藏底部工具栏
     */
    private fun hideToolbar() {
        // 如果 Toolbar 隐藏或将要隐藏, 则跳过
        if (!toolbarShownOrWillShow) {
            return
        }
        toolbarShownOrWillShow = false
        val delta = toolbar?.height ?: 0
        val oldY = toolbar?.y ?: 0f
        val newY = oldY + delta
        val anim = ObjectAnimator.ofFloat(toolbar!!, "y", oldY, newY)
        anim.interpolator = AccelerateDecelerateInterpolator()
        anim.duration = 200
        anim.addListener(object : Animator.AnimatorListener {
            override fun onAnimationRepeat(p0: Animator?) = Unit

            override fun onAnimationEnd(p0: Animator?) {
                fab?.show()
                // 将 toolbar 置于默认的状态
                toolbar?.visibility = View.INVISIBLE
                toolbar?.y = oldY
            }

            override fun onAnimationCancel(p0: Animator?) = Unit

            override fun onAnimationStart(p0: Animator?) = Unit
        })
        anim.start()
    }

    /**
     * 显示底部工具栏
     */
    private fun showToolbar() {
        // 如果 toolbar 显示或将要显示, 则跳过
        if (toolbarShownOrWillShow) {
            return
        }
        toolbarShownOrWillShow = true
        val delta = toolbar?.height ?: 0
        val newY = toolbar?.y ?: 0f
        val oldY = newY + delta
        val anim = ObjectAnimator.ofFloat(toolbar!!, "y", oldY, newY)
        anim.interpolator = AccelerateDecelerateInterpolator()
        anim.duration = 200
        anim.addListener(object : Animator.AnimatorListener {
            override fun onAnimationRepeat(p0: Animator?) = Unit

            override fun onAnimationEnd(p0: Animator?) = Unit

            override fun onAnimationCancel(p0: Animator?) = Unit

            override fun onAnimationStart(p0: Animator?) {
                toolbar?.visibility = View.VISIBLE
            }
        })
        anim.start()
    }

    override fun onDestroy() {
        disposable.clear()
        super.onDestroy()
    }

    /**
     * Fab 的显隐监听
     * 隐藏 Fab 时显示 StoryFragment 的 Toolbar
     */
    val fabListener = object : FloatingActionButton.OnVisibilityChangedListener() {
        override fun onShown(fab: FloatingActionButton?) {
            super.onShown(fab)
        }

        override fun onHidden(fab: FloatingActionButton?) {
            super.onHidden(fab)
            showToolbar()
        }
    }

    companion object {
        val ID = "id"
        val REQUEST_EDIT = 100

        fun getStoryIntent(context: Context, id: Long): Intent {
            val intent = Intent(context, StoryDetailActivity::class.java)
            intent.putExtra(ID, id)
            return intent
        }
    }
}
