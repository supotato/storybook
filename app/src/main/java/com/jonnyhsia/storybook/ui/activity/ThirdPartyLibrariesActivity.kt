package com.jonnyhsia.storybook.ui.activity

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import com.jonnyhsia.storybook.R
import com.jonnyhsia.storybook.ui.multitype.LibraryViewBinder
import kotlinx.android.synthetic.main.activity_third_party_libraries.rvLibs
import kotlinx.android.synthetic.main.activity_third_party_libraries.toolbar
import me.drakeet.multitype.Items
import me.drakeet.multitype.MultiTypeAdapter

class ThirdPartyLibrariesActivity : DayNightActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_third_party_libraries)

        setSupportActionBar(toolbar)
        supportActionBar?.title = ""
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setHomeButtonEnabled(true)

        initRecyclerView()
    }

    private val items = Items()
    private var rvAdapter: MultiTypeAdapter? = null

    private fun initRecyclerView() {
        items.addAll(resources.getStringArray(R.array.third_party_libs))
        rvAdapter = MultiTypeAdapter(items)
        rvAdapter?.register(String::class.java, LibraryViewBinder())

        rvLibs?.setHasFixedSize(true)
        rvLibs?.adapter = rvAdapter
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            android.R.id.home -> {
                finish()
            }
        }
        return super.onOptionsItemSelected(item)
    }
}
