package com.jonnyhsia.storybook.ui.activity

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentPagerAdapter
import android.support.v4.view.ViewPager
import android.view.View
import com.jonnyhsia.storybook.R
import com.jonnyhsia.storybook.ui.fragment.LoginSignUpFragment
import com.jonnyhsia.storybook.ui.fragment.ShowcaseFragment
import com.jonnyhsia.storybook.utils.Preference
import com.jonnyhsia.storybook.utils.jump2Activity
import kotlinx.android.synthetic.main.activity_welcome.indicator
import kotlinx.android.synthetic.main.activity_welcome.tvSkip
import kotlinx.android.synthetic.main.activity_welcome.viewPager

class WelcomeActivity : DayNightActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_welcome)

        initView()
    }

    private fun initView() {
        val fragments = arrayOf(
                ShowcaseFragment.newInstance("写下属于你的故事", "巴拉巴拉巴拉巴拉巴拉巴拉巴拉巴拉", R.mipmap.showcase_timeline),
                ShowcaseFragment.newInstance("写下属于你的故事", "巴拉巴拉巴拉巴拉巴拉巴拉巴拉巴拉", R.mipmap.showcase_discover),
                LoginSignUpFragment())
        val adapter = object : FragmentPagerAdapter(supportFragmentManager) {
            override fun getItem(position: Int): Fragment {
                return fragments[position]
            }

            override fun getCount() = fragments.size
        }
        viewPager.adapter = adapter
        viewPager.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrollStateChanged(state: Int) {}

            override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {}

            override fun onPageSelected(position: Int) {
                tvSkip?.visibility = if (position == adapter.count - 1) {
                    View.VISIBLE
                } else {
                    View.GONE
                }
            }

        })
        indicator.setViewPager(viewPager)

        tvSkip?.setOnClickListener {
            var username by Preference(this, Preference.USERNAME, "")
            username = "skip"
            finish()
            jump2Activity(MainActivity::class.java)
        }
    }
}
