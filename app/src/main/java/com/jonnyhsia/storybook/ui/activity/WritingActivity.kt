package com.jonnyhsia.storybook.ui.activity

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import com.jonnyhsia.storybook.exception.HttpRequestFailedException
import com.jonnyhsia.storybook.App
import com.jonnyhsia.storybook.R
import com.jonnyhsia.storybook.persistence.Story
import com.jonnyhsia.storybook.http.Api
import com.jonnyhsia.storybook.http.RetrofitFactory
import com.jonnyhsia.storybook.persistence.StorybookDatabase
import com.jonnyhsia.storybook.rx.RxHttpSchedulers
import com.jonnyhsia.storybook.utils.Preference
import com.jonnyhsia.storybook.utils.Utils
import com.jonnyhsia.storybook.utils.loge
import com.jonnyhsia.storybook.utils.toast
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.functions.Action
import io.reactivex.internal.operators.completable.CompletableFromAction
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_writing.*
import java.util.Date

class WritingActivity : DayNightActivity() {

    private var storyId = -1L
    private var storyTitle: String? = null
    private var storyContent: String? = null
    private var imgs: ArrayList<String>? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_writing)

        initView()
    }

    private fun initView() {
        inputTitle.requestFocus()
        if (intent.hasExtra(CONTENT)) {
            storyTitle = intent.getStringExtra(TITLE)
            storyContent = intent.getStringExtra(CONTENT)
            imgs = intent.getStringArrayListExtra(IMGS)
            storyId = intent.getLongExtra(ID, -1L)
            inputTitle?.setText(storyTitle)
            inputContent?.setText(storyContent)
            tvPublish?.text = getString(R.string.save_story_changes)
        }
        tvWordsCount?.text = getString(R.string.words_count, inputContent?.editableText?.length.toString())

        imgClose?.setOnClickListener {
            onBackPressed()
        }
        tvPublish?.setOnClickListener {
            publish()
        }
        toolPreviousLetter?.setOnClickListener {
            previousLetter()
        }
        toolNextLetter?.setOnClickListener {
            nextLetter()
        }

        inputContent?.typeface = App.TYPEFACE
        inputContent?.addTextChangedListener(textWatcher)
    }

    private fun previousLetter() {
        val editText = viewInput?.findFocus() as? EditText ?: return
        val index = maxOf(0, editText.selectionStart - 1)
        editText.setSelection(index)
    }

    private fun nextLetter() {
        val editText = viewInput?.findFocus() as? EditText ?: return
        val index = minOf(editText.text.count(), editText.selectionStart + 1)
        editText.setSelection(index)
    }

    /**
     * 发布故事
     */
    private fun publish() {
        tvPublish.isEnabled = false

        val title = inputTitle?.text.toString()
        val content = inputContent?.text.toString()
        val author by Preference(this, Preference.USERNAME, "")
        val map = mapOf(
                "story_id" to storyId.toString(),
                "title" to title,
                "content" to content,
                "imgs" to "",
                "author" to author
        )

        val api = RetrofitFactory.build(Api.BASE_URL).create(Api::class.java)
        api.publishStory(map)
                .compose(RxHttpSchedulers.composeSingle())
                .doOnSuccess {
                    if (it.success) {
                        toast("${it.data}: 服务器发布/保存成功")
                    } else {
                        throw HttpRequestFailedException(it.error)
                    }
                }
                .observeOn(Schedulers.io())
                .flatMapCompletable { response ->
                    CompletableFromAction(Action {
                        StorybookDatabase.instance(this).storyDao()
                                .insertStory(Story(response.data!!, title, content, author, "", Date()))
                    })
                }
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    val data = Intent()
                    data.putExtra(TITLE, inputTitle?.text.toString())
                    data.putExtra(CONTENT, inputContent?.text.toString())
                    data.putExtra(IMGS, "")
                    setResult(RESULT_OK, data)
                    onBackPressed()
                }, { error ->
                    toast("本地发生错误, 请重试.\n${error.message}")
                    loge(error.message, error)
                    tvPublish.isEnabled = true
                })
    }

    override fun onBackPressed() {
        val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(window.decorView.windowToken, 0)
        // TODO 若有内容则提醒用户是否保存草稿
        super.onBackPressed()
    }

    private val textWatcher = object : TextWatcher {
        override fun afterTextChanged(editable: Editable?) {
            if (editable == null) return
            val cnt = (Utils.deleteBlankSpace(editable.toString())).length
            tvWordsCount?.text = getString(R.string.words_count, ": $cnt")
        }

        override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}

        override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}

    }

    companion object {
        val TITLE = "title"
        val CONTENT = "content"
        val IMGS = "imgs"
        val ID = "id"

        fun getWritingIntent(context: Context, story: Story): Intent {
            return getWritingIntent(context, story.storyId, story.title, story.content, story.imagesArray())
        }

        private fun getWritingIntent(context: Context, id: Long, title: String, content: String, imgArray: ArrayList<String>?): Intent {
            val intent = Intent(context, WritingActivity::class.java)
            intent.putExtra(ID, id)
            intent.putExtra(TITLE, title)
            intent.putExtra(CONTENT, content)
            intent.putExtra(IMGS, imgArray)
            return intent
        }
    }
}
