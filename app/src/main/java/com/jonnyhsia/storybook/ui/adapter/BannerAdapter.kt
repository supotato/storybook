package com.jonnyhsia.storybook.ui.adapter

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.jonnyhsia.storybook.R
import com.jonnyhsia.storybook.persistence.Banner
import com.jonnyhsia.storybook.ui.CardOutlineProvider
import com.jonnyhsia.storybook.ui.SpannedGridLayoutManager
import com.jonnyhsia.storybook.utils.UIUtils
import com.jonnyhsia.storybook.utils.find

/**
 * Created by JonnyHsia on 17/9/12.
 */
class BannerAdapter(private var adapterData: ArrayList<Banner>) : RecyclerView.Adapter<BannerAdapter.VH>() {

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): VH {
        val view = LayoutInflater.from(parent?.context)
                .inflate(R.layout.item_banner_collection, parent, false)
        return VH(view)
    }

    override fun onBindViewHolder(holder: VH?, position: Int) {
        holder?.bind(adapterData[position])
        holder?.itemView?.outlineProvider = CardOutlineProvider(alpha = 0.5f)
    }

    override fun getItemCount(): Int = adapterData.size

    inner class VH(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private val rvCollection = find<RecyclerView>(R.id.rvCollection)

        fun bind(data: Banner) {
            rvCollection?.setHasFixedSize(true)
            rvCollection?.layoutManager = SpannedGridLayoutManager(SpannedGridLayoutManager.GridSpanLookup { position ->
                when (position) {
                    0, 2 -> SpannedGridLayoutManager.SpanInfo(2, 2)
                    else -> SpannedGridLayoutManager.SpanInfo(1, 1)
                }
            }, 5, 1f)
            rvCollection?.adapter = CollectionAdapter(data.imgs.asList())
        }
    }

    fun updateData(newData: ArrayList<Banner>) {
        if (adapterData != newData) {
            adapterData = newData
            notifyDataSetChanged()
        }
    }
}