package com.jonnyhsia.storybook.ui.adapter

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import com.bumptech.glide.load.MultiTransformation
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.resource.bitmap.CenterCrop
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.bumptech.glide.request.RequestOptions
import com.bumptech.glide.request.transition.DrawableCrossFadeTransition
import com.jonnyhsia.storybook.GlideApp
import com.jonnyhsia.storybook.R
import com.jonnyhsia.storybook.ui.glide.GlideRoundTransform

/**
 * Created by JonnyHsia on 17/9/13.
 */
class CollectionAdapter(private var adapterData: List<String>)
    : RecyclerView.Adapter<CollectionAdapter.CollectionViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): CollectionViewHolder? {
        val view = LayoutInflater.from(parent?.context).inflate(R.layout.item_image_collection, parent, false)
        return CollectionViewHolder(view)
    }

    override fun onBindViewHolder(holder: CollectionViewHolder?, position: Int) {
        holder?.bind(adapterData[position])
    }

    override fun getItemCount() = adapterData.size

    inner class CollectionViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bind(imageUrl: String) {
            GlideApp.with(itemView)
                    .load(imageUrl)
                    .transform(MultiTransformation(CenterCrop(), GlideRoundTransform(itemView.context, 4)))
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(itemView as? ImageView)
        }

    }
}