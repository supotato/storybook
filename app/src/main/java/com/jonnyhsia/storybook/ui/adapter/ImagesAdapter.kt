package com.jonnyhsia.storybook.ui.adapter

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.jonnyhsia.storybook.GlideApp
import com.jonnyhsia.storybook.R
import com.jonnyhsia.storybook.utils.find

/**
 * Created by JonnyHsia on 17/8/11.
 */
class ImagesAdapter(private var adapterData: ArrayList<String>)
    : RecyclerView.Adapter<ImagesAdapter.VH>() {

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): VH {
        val view = LayoutInflater.from(parent?.context)
                .inflate(R.layout.item_image, parent, false)
        return VH(view)
    }

    override fun onBindViewHolder(holder: VH?, position: Int) {
        holder?.bind(adapterData[position], position, itemCount)
    }

    override fun getItemCount() = adapterData.size

    fun updateData(newData: ArrayList<String>) {
        if (adapterData != newData) {
            adapterData = newData
            notifyDataSetChanged()
        }
    }

    inner class VH(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private var image = find<ImageView>(R.id.imgItemStory)
        private var tvCount = find<TextView>(R.id.tvItemImgCnt)

        fun bind(img: String, pos: Int, cnt: Int) {
            GlideApp.with(itemView.context)
                    .load(img)
                    .into(image)
            tvCount?.text = if (cnt == 1) {
                "${pos + 1}"
            } else {
                "${pos + 1}/$cnt"
            }
        }
    }
}