package com.jonnyhsia.storybook.ui.adapter

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.jonnyhsia.storybook.GlideApp
import com.jonnyhsia.storybook.R

/**
 * Created by JonnyHsia on 17/9/15.
 */
class ShowcaseAdapter(private var showcase: ArrayList<Int>) : RecyclerView.Adapter<ShowcaseAdapter.VH>() {

    override fun getItemCount() = showcase.size

    override fun onBindViewHolder(holder: VH?, position: Int) {
        val image = holder?.itemView as? ImageView ?: return
        GlideApp.with(holder.itemView)
                .load(showcase[position])
                .centerCrop()
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(image)
    }

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): VH {
        val view = LayoutInflater.from(parent?.context)
                .inflate(R.layout.item_showcase, parent, false)
        return VH(view)
    }


    inner class VH(itemView: View) : RecyclerView.ViewHolder(itemView)
}