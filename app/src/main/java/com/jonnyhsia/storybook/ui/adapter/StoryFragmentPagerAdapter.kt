package com.jonnyhsia.storybook.ui.adapter

import android.annotation.SuppressLint
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import android.support.v4.app.FragmentStatePagerAdapter
import android.support.v4.view.PagerAdapter
import android.view.ViewGroup
import com.jonnyhsia.storybook.persistence.Story
import com.jonnyhsia.storybook.ui.fragment.StoryFragment
import com.jonnyhsia.storybook.utils.CompatUtils

/**
 * Created by JonnyHsia on 17/8/28.
 * 故事 ViewPager 适配器
 */
class StoryFragmentPagerAdapter(fm: FragmentManager,
                                private var storyData: ArrayList<Story>) : FragmentStatePagerAdapter(fm) {

    var currentFragment: StoryFragment? = null

    override fun getItem(position: Int): Fragment {
        val fragmentData = storyData[position]
        return StoryFragment.newInstance(fragmentData)
    }

    override fun getCount(): Int {
        return storyData.size
    }

    /**
     * 设置 PrimaryItem 用以随时获取 currentFragment
     */
    override fun setPrimaryItem(container: ViewGroup?, position: Int, obj: Any?) {
        currentFragment = obj as? StoryFragment
        super.setPrimaryItem(container, position, obj)
    }

    override fun getItemPosition(obj: Any?): Int {
        return PagerAdapter.POSITION_NONE
    }

    /**
     * 更新故事数据
     */
    fun updateStoryData(data: ArrayList<Story>) {
        storyData = data
        notifyDataSetChanged()
    }

    /**
     * 获取指定位置的数据
     */
    fun adapterData(pos: Int): Story? {
        return if (pos in 0 until storyData.size)
            storyData[pos]
        else
            null
    }

    /**
     * 删除故事
     */
    @SuppressLint("NewApi")
    fun deleteStory(story: Story) {
        CompatUtils.supportJ8({
            storyData.removeIf { it.storyId == story.storyId }
        }, {
            storyData.remove(story)
        })
        notifyDataSetChanged()
    }

    /**
     * 更新指定页的故事数据
     * @param pos 更新的页
     */
    fun updateOneStory(pos: Int, newTitle: String, newContent: String, newImgs: String) {
        with(storyData[pos]) {
            title = newTitle
            content = newContent
            images = newImgs
        }
        currentFragment?.notifyStoryChanged(newTitle, newContent, newImgs)
    }
}