package com.jonnyhsia.storybook.ui.adapter

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.load.MultiTransformation
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.resource.bitmap.CenterCrop
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.jonnyhsia.storybook.GlideApp
import com.jonnyhsia.storybook.R
import com.jonnyhsia.storybook.persistence.Subscription
import com.jonnyhsia.storybook.ui.glide.GlideRoundTransform
import com.jonnyhsia.storybook.utils.find

/**
 * 故事订阅的适配器
 * Created by JonnyHsia on 17/9/13.
 */
class SubscriptionAdapter(var adapterData: ArrayList<Subscription>)
    : RecyclerView.Adapter<SubscriptionAdapter.SubscriptionViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): SubscriptionViewHolder {
        val view = LayoutInflater.from(parent?.context).inflate(R.layout.item_subscription, parent, false)
        return SubscriptionViewHolder(view)
    }

    override fun onBindViewHolder(holder: SubscriptionViewHolder?, position: Int) {
        val data = adapterData[position]
        holder?.tvTitle?.text = data.title
        GlideApp.with(holder?.itemView)
                .load(data.img)
                .transforms(arrayOf(CenterCrop(), GlideRoundTransform(holder?.itemView?.context, 6)))
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(holder?.imgSubscription)
    }

    override fun getItemCount() = adapterData.size

    class SubscriptionViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val tvTitle = find<TextView>(R.id.tvSubscriptionTitle)
        val imgSubscription = find<ImageView>(R.id.imgSubscription)

    }

    fun updateData(newData: ArrayList<Subscription>) {
        if (adapterData != newData) {
            adapterData = newData
            notifyDataSetChanged()
        }
    }
}

