package com.jonnyhsia.storybook.ui.adapter

import android.annotation.SuppressLint
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.jonnyhsia.storybook.R
import com.jonnyhsia.storybook.persistence.Story
import com.jonnyhsia.storybook.ui.viewholder.TimelineHolder
import com.jonnyhsia.storybook.ui.viewholder.TimelineImageHolder
import java.text.SimpleDateFormat
import java.util.*

/**
 * Created by JonnyHsia on 17/8/3.
 * 时间线适配器
 */
class TimelineAdapter(var adapterData: ArrayList<Story>)
    : RecyclerView.Adapter<TimelineHolder>() {

    private val dateFormat = SimpleDateFormat("MMdd", Locale.CHINA)

    companion object {
        // ViewType 类型常量
        val TYPE_TEXT = 0
        val TYPE_IMAGE = 1
    }

    override fun getItemViewType(position: Int): Int {
        // 根据是否有 header 和 data 是否有图片返回对应的 ViewType
        return when {
            adapterData[position].images.isNotEmpty() -> TYPE_IMAGE
            else -> TYPE_TEXT
        }
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: TimelineHolder?, position: Int) {
        val item = adapterData[position]
        holder?.tvTitle?.text = "${dateFormat.format(item.createTime)}  ${item.title}"
        holder?.bind(item)
    }

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): TimelineHolder {
        val view: View
        val holder: TimelineHolder
        // 根据不同的 ViewType 创建不同的 BaseViewHolder
        when (viewType) {
            TYPE_TEXT -> {
                view = LayoutInflater.from(parent?.context).inflate(R.layout.item_timeline, parent, false)
                holder = TimelineHolder(view)
            }
            TYPE_IMAGE -> {
                view = LayoutInflater.from(parent?.context).inflate(R.layout.item_timeline_image, parent, false)
                holder = TimelineImageHolder(view)
            }
            else -> {
                throw Exception("ViewType can only be PureText or ImageText.")
            }
        }
        return holder
    }

    override fun getItemCount(): Int {
        return adapterData.size
    }

    /**
     * 更新 Adapter 数据
     */
    fun updateData(data: ArrayList<Story>) {
        if (adapterData != data) {
            adapterData = data
            notifyDataSetChanged()
        }
    }
}