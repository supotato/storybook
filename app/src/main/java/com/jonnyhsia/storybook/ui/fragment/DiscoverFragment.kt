package com.jonnyhsia.storybook.ui.fragment

import android.animation.ObjectAnimator
import android.os.Bundle
import android.support.design.widget.BottomSheetBehavior
import android.support.v4.app.Fragment
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.github.rubensousa.gravitysnaphelper.GravityPagerSnapHelper

import com.jonnyhsia.storybook.R
import com.jonnyhsia.storybook.persistence.Banner
import com.jonnyhsia.storybook.persistence.Subscription
import com.jonnyhsia.storybook.ui.Scroll2Top
import com.jonnyhsia.storybook.ui.adapter.BannerAdapter
import com.jonnyhsia.storybook.ui.multitype.Selection
import com.jonnyhsia.storybook.ui.multitype.SelectionViewBinder
import com.jonnyhsia.storybook.ui.multitype.SubscriptionViewBinder
import com.jonnyhsia.storybook.utils.UIUtils
import kotlinx.android.synthetic.main.fragment_discover.divider
import kotlinx.android.synthetic.main.fragment_discover.stub_banner
import kotlinx.android.synthetic.main.fragment_discover.stub_subscription
import kotlinx.android.synthetic.main.fragment_discover.viewLoading
import me.drakeet.multitype.Items
import me.drakeet.multitype.MultiTypeAdapter

class DiscoverFragment : Fragment(), Scroll2Top {

    private var behavior: BottomSheetBehavior<View>? = null
    private var bannersData = ArrayList<Banner>()
    private var bannerAdapter: BannerAdapter? = null
    private var subscriptionAdapter: MultiTypeAdapter? = null
    private var subscriptionData = Items()

    private var rvBanner: RecyclerView? = null
    private var rvSubscription: RecyclerView? = null

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater?.inflate(R.layout.fragment_discover, container, false)
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initView()
    }

    private fun initView() {
        bannerAdapter = BannerAdapter(bannersData)
        subscriptionAdapter = MultiTypeAdapter(subscriptionData)
        subscriptionAdapter?.register(Selection::class.java, SelectionViewBinder())
        subscriptionAdapter?.register(Subscription::class.java, SubscriptionViewBinder())

        initData()
    }

    private fun initData() {
        Thread(Runnable {
            Thread.sleep(1600)
            bannersData = fakeData()
            subscriptionData.add(Selection("我的订阅", "编辑"))
            subscriptionData.addAll(fakeSubscription())
            activity?.runOnUiThread {
                viewLoading?.visibility = View.GONE
                subscriptionAdapter?.items = subscriptionData
                initBanner()
                initSubscription()
                divider?.post {
                    view?.height?.let { behavior?.peekHeight = (it - divider.y - UIUtils.dp2px(1f, activity)).toInt() }
                }
            }
        }).start()
    }

    private fun initSubscription() {
        if (rvSubscription == null) {
            rvSubscription = stub_subscription?.inflate() as RecyclerView?
            val span = 3
            val layoutManager = GridLayoutManager(activity, span)
            layoutManager.spanSizeLookup = object : GridLayoutManager.SpanSizeLookup() {
                override fun getSpanSize(position: Int): Int {
                    val item = subscriptionData[position]
                    return if (item is Selection) span else 1
                }
            }

            rvSubscription?.setHasFixedSize(true)
            rvSubscription?.layoutManager = layoutManager
            rvSubscription?.adapter = subscriptionAdapter

            behavior = BottomSheetBehavior.from(rvSubscription)
            behavior?.setBottomSheetCallback(object : BottomSheetBehavior.BottomSheetCallback() {
                override fun onSlide(bottomSheet: View, slideOffset: Float) {}

                override fun onStateChanged(bottomSheet: View, newState: Int) {
                    when (newState) {
                        BottomSheetBehavior.STATE_COLLAPSED, BottomSheetBehavior.STATE_EXPANDED -> {
                            rvSubscription?.animate()?.translationZ(0f)
                        }
                        BottomSheetBehavior.STATE_DRAGGING -> {
                            rvSubscription?.animate()?.translationZ(UIUtils.dp2px(16f, activity))
                        }
                    }
                }
            })
        }
        subscriptionAdapter?.notifyDataSetChanged()
        ObjectAnimator.ofFloat(rvSubscription!!, "alpha", 0f, 1f).start()
    }

    private fun initBanner() {
        if (rvBanner == null) {
            rvBanner = stub_banner?.inflate() as RecyclerView?

            rvBanner?.setHasFixedSize(true)
            rvBanner?.layoutManager = LinearLayoutManager(activity, LinearLayoutManager.HORIZONTAL, false)
            rvBanner?.adapter = bannerAdapter
            GravityPagerSnapHelper(Gravity.START).attachToRecyclerView(rvBanner)
        }
        bannerAdapter?.updateData(bannersData)
        ObjectAnimator.ofFloat(rvBanner!!, "alpha", 0f, 1f).start()
    }

    private fun fakeSubscription(): ArrayList<Subscription> {
        val subscription = ArrayList<Subscription>()
        val imgs = arrayOf("http://hencoder.com/content/images/2017/06/HenCoder-icon.png",
                "http://ou4f31a1x.bkt.clouddn.com/17-8-4/56105663.jpg",
                "https://ooo.0o0.ooo/2017/06/29/5953e98ea97d1.jpg",
                "https://ooo.0o0.ooo/2017/07/03/5959a42837152.png",
                "https://ss2.bdstatic.com/70cFvnSh_Q1YnxGkpoWK1HF6hhy/it/u=2561653641,1079686721&fm=11&gp=0.jpg",
                "http://ou4f31a1x.bkt.clouddn.com/17-8-4/78293026.jpg",
                "https://ooo.0o0.ooo/2017/06/29/5953f4e0c3c1a.jpg",
                "http://ou4f31a1x.bkt.clouddn.com/17-8-4/38058011.jpg",
                "https://ooo.0o0.ooo/2017/06/29/59548e09f1fa3.jpg",
                "http://ou4f31a1x.bkt.clouddn.com/17-8-4/56105663.jpg",
                "https://ooo.0o0.ooo/2017/06/29/5953e98ea97d1.jpg")
        (0..10).mapTo(subscription, {
            Subscription(0, "标题 $it", "", imgs[it])
        })
        return subscription
    }

    private fun fakeData(): ArrayList<Banner> {
        val data = ArrayList<Banner>()
        val imgs = arrayOf("http://ou4f31a1x.bkt.clouddn.com/17-9-13/19121933.jpg",
                "http://ou4f31a1x.bkt.clouddn.com/17-8-4/56105663.jpg",
                "http://ou4f31a1x.bkt.clouddn.com/17-8-4/38058011.jpg",
                "https://ooo.0o0.ooo/2017/06/29/5953e98ea97d1.jpg",
                "https://ooo.0o0.ooo/2017/06/29/59548e09f1fa3.jpg",
                "https://ooo.0o0.ooo/2017/06/29/5953f4e0c3c1a.jpg",
                "http://ou4f31a1x.bkt.clouddn.com/17-8-4/38058011.jpg",
                "https://ss2.bdstatic.com/70cFvnSh_Q1YnxGkpoWK1HF6hhy/it/u=2561653641,1079686721&fm=11&gp=0.jpg",
                "http://ou4f31a1x.bkt.clouddn.com/17-9-13/7098498.jpg")
        (0..5).mapTo(data, { cnt: Int ->
            Banner(contentUrl = "",
                    title = "Title: $cnt",
                    msg = "Msg: $cnt",
                    imgs = imgs)
        })
        return data
    }

    override fun scroll2Top() {
        rvSubscription?.scrollToPosition(0)
        if (behavior?.state != BottomSheetBehavior.STATE_COLLAPSED) {
            behavior?.state = BottomSheetBehavior.STATE_COLLAPSED
        }
    }
}
