package com.jonnyhsia.storybook.ui.fragment

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.jonnyhsia.storybook.R
import com.jonnyhsia.storybook.entity.User
import com.jonnyhsia.storybook.http.Api
import com.jonnyhsia.storybook.http.RetrofitFactory
import com.jonnyhsia.storybook.rx.RxHttpSchedulers
import com.jonnyhsia.storybook.ui.activity.MainActivity
import com.jonnyhsia.storybook.utils.Preference
import com.jonnyhsia.storybook.utils.httpLog
import com.jonnyhsia.storybook.utils.jump2Activity
import com.jonnyhsia.storybook.utils.setTintDrawable
import com.jonnyhsia.storybook.utils.textString
import com.jonnyhsia.storybook.utils.toast
import kotlinx.android.synthetic.main.fragment_login.inputPassword
import kotlinx.android.synthetic.main.fragment_login.inputUsername
import kotlinx.android.synthetic.main.fragment_login.tvLogin
import kotlinx.android.synthetic.main.fragment_login.tvSignUp
import retrofit2.HttpException

/**
 * 登录 [Fragment].
 */
class LoginFragment : Fragment() {

    var trySignUp: ((Unit) -> Unit)? = null

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater?.inflate(R.layout.fragment_login, container, false)
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        tvLogin?.setTintDrawable(resources, R.mipmap.ic_chevron_right, R.color.text_primary)
        tvSignUp?.setOnClickListener {
            trySignUp?.invoke(Unit)
        }
        tvLogin?.setOnClickListener {
            requestLogin(inputUsername.textString(),
                    inputPassword.textString())
        }
    }

    /**
     * 请求登录账号
     */
    private fun requestLogin(username: String, password: String) {
        val api = RetrofitFactory.build(Api.BASE_URL).create(Api::class.java)
        api.login(username, password)
                .compose(RxHttpSchedulers.composeObservable())
                .subscribe({ response ->
                    if (response == null || !response.success || response.data == null) {
                        toast("登录失败: ${response.error}")
                    } else {
                        toast("登录成功")
                        // 保存数据, 并跳转至首页
                        prepareLogin(response.data!!)
                        activity?.finish()
                        jump2Activity(MainActivity::class.java)
                    }
                }) {
                    (it as? HttpException)?.let { toast(it.response().errorBody()?.string().toString()) }
                    it.httpLog()
                }
    }

    /**
     * 将用户名保存的本地
     */
    private fun prepareLogin(user: User) {
        var username by Preference(activity, Preference.USERNAME, "")
        var nickname by Preference(activity, Preference.NICKNAME, "")
        var avatar by Preference(activity, Preference.AVATAR, "")
        var email by Preference(activity, Preference.EMAIL, "")
        username = user.username
        nickname = user.nickname
        avatar = user.avatar
        email = user.email
    }

}
