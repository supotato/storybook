package com.jonnyhsia.storybook.ui.fragment


import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.jonnyhsia.storybook.R


/**
 * A simple [Fragment] subclass.
 */
class LoginSignUpFragment : Fragment() {

    companion object {
        val SIGN_UP = 0
        val LOGIN = 1
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater?.inflate(R.layout.fragment_login_sign_up, container, false)
    }

    private val signUpFragment by lazy { SignUpFragment() }
    private val loginFragment by lazy { LoginFragment() }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        signUpFragment.tryLogin = {
            loginOrSignUp(LOGIN)
        }

        loginFragment.trySignUp = {
            loginOrSignUp(SIGN_UP)
        }

        childFragmentManager.beginTransaction()
                .replace(R.id.container, signUpFragment)
                .commit()
    }

    private fun loginOrSignUp(flag: Int) {
        val animEnter: Int
        val animExit: Int
        val fragment: Fragment

        when (flag) {
            SIGN_UP -> {
                animEnter = R.anim.popup_enter_reverse
                animExit = R.anim.popup_exit
                fragment = signUpFragment
            }
            LOGIN -> {
                animEnter = R.anim.popup_enter
                animExit = R.anim.popup_exit
                fragment = loginFragment
            }
            else -> throw IllegalArgumentException("Flag is invalid!")
        }

        childFragmentManager.beginTransaction()
                .setCustomAnimations(animEnter, animExit)
                .replace(R.id.container, fragment)
                .commit()
    }

}
