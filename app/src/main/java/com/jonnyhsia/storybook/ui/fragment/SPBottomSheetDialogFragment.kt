package com.jonnyhsia.storybook.ui.fragment;

import android.app.Dialog
import android.os.Bundle
import android.support.design.widget.BottomSheetDialogFragment
import android.view.View

import android.support.design.widget.BottomSheetBehavior
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import com.jonnyhsia.storybook.GlideApp
import com.jonnyhsia.storybook.R
import com.jonnyhsia.storybook.entity.SheetModel
import kotlin.properties.Delegates

/**
 * BottomSheetDialog
 */
class SPBottomSheetDialogFragment : BottomSheetDialogFragment() {

    private var behavior: BottomSheetBehavior<View>? = null

    var onClick: ((Unit) -> Unit)? = null
    var title: String? = null
    var content: String? = null
    var imageUrl: String? = null

    private var tvTitle: TextView by Delegates.notNull()
    private var tvContent: TextView by Delegates.notNull()
    private var imageSheet: ImageView by Delegates.notNull()
    private var btnAction: Button by Delegates.notNull()

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val dialog = super.onCreateDialog(savedInstanceState)
        // 创建 Dialog 的内容布局
        val view = View.inflate(context, R.layout.dialog_sheet, null)
        dialog.setContentView(view)
        // 设置 Dialog 的 UI 与点击监听
        title = arguments.getString(TITLE)
        content = arguments.getString(CONTENT)
        imageUrl = arguments.getString(IMAGE)

        btnAction = view.findViewById<Button>(R.id.btnAction).apply {
            text = arguments.getString(ACTION)
            setOnClickListener {
                onClick?.invoke(Unit)
                dismiss()
            }
        }
        tvTitle = view.findViewById<TextView>(R.id.tvSheetTitle).apply {
            text = title
        }
        tvContent = view.findViewById<TextView>(R.id.tvSheetContent).apply {
            text = content
        }
        imageSheet = view.findViewById<ImageView>(R.id.imgSheet).apply {
            visibility = if (!imageUrl.isNullOrEmpty()) View.VISIBLE else View.GONE
        }
        GlideApp.with(view)
                .load(imageUrl)
                .into(imageSheet)


        // 获取 Dialog 内容布局的 parent
        val root = view.parent as? View
        // 设置 root 的背景色为透明, 并获取其 behavior
        root?.setBackgroundColor(resources.getColor(android.R.color.transparent))
        behavior = BottomSheetBehavior.from(root)

        return dialog
    }

    override fun onStart() {
        super.onStart()
        // 展开 BottomSheet
        behavior?.state = BottomSheetBehavior.STATE_EXPANDED
    }

    override fun dismiss() {
        // 收缩 BottomSheet
        behavior?.state = BottomSheetBehavior.STATE_HIDDEN
        super.dismiss()
    }

    companion object {
        val TITLE = "title"
        val CONTENT = "content"
        val IMAGE = "image"
        val ACTION = "action"

        fun newInstance(model: SheetModel): SPBottomSheetDialogFragment {
            return newInstance(model.title, model.content, model.image, model.action)
        }

        fun newInstance(title: String, content: String, image: String, action: String)
                : SPBottomSheetDialogFragment {
            val args = Bundle()
            args.putString(TITLE, title)
            args.putString(CONTENT, content)
            args.putString(IMAGE, image)
            args.putString(ACTION, action)

            return SPBottomSheetDialogFragment().apply { arguments = args }
        }
    }
}
