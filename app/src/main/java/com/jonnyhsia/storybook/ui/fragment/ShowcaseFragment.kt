package com.jonnyhsia.storybook.ui.fragment


import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.jonnyhsia.storybook.GlideApp

import com.jonnyhsia.storybook.R
import kotlinx.android.synthetic.main.fragment_showcase.icShowcase
import kotlinx.android.synthetic.main.fragment_showcase.tvContent
import kotlinx.android.synthetic.main.fragment_showcase.tvTitle


/**
 * A simple [Fragment] subclass.
 */
class ShowcaseFragment : Fragment() {

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater?.inflate(R.layout.fragment_showcase, container, false)
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        tvTitle?.text = arguments?.getString(ARGS_TITLE)
        tvContent?.text = arguments?.getString(ARGS_CONTENT)
        GlideApp.with(this)
                .load(arguments?.getInt(ARGS_ICON))
                .into(icShowcase)
    }

    companion object {
        val ARGS_TITLE = "title"
        val ARGS_CONTENT = "content"
        val ARGS_ICON = "icon"

        fun newInstance(title: String,
                        content: String,
                        iconRes: Int): ShowcaseFragment {
            val instance = ShowcaseFragment()

            val args = Bundle()
            args.putString(ARGS_TITLE, title)
            args.putString(ARGS_CONTENT, content)
            args.putInt(ARGS_ICON, iconRes)
            instance.arguments = args

            return instance
        }
    }

}
