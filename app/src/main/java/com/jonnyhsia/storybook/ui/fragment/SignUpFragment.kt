package com.jonnyhsia.storybook.ui.fragment


import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.jonnyhsia.storybook.R
import com.jonnyhsia.storybook.entity.User
import com.jonnyhsia.storybook.http.Api
import com.jonnyhsia.storybook.http.Response
import com.jonnyhsia.storybook.http.RetrofitFactory
import com.jonnyhsia.storybook.rx.RxHttpSchedulers
import com.jonnyhsia.storybook.ui.activity.MainActivity
import com.jonnyhsia.storybook.utils.Preference
import com.jonnyhsia.storybook.utils.httpLog
import com.jonnyhsia.storybook.utils.jump2Activity
import com.jonnyhsia.storybook.utils.setTintDrawable
import com.jonnyhsia.storybook.utils.textString
import com.jonnyhsia.storybook.utils.toast
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.fragment_sign_up.inputEmail
import kotlinx.android.synthetic.main.fragment_sign_up.inputPassword
import kotlinx.android.synthetic.main.fragment_sign_up.inputUsername
import kotlinx.android.synthetic.main.fragment_sign_up.tvLogin
import kotlinx.android.synthetic.main.fragment_sign_up.tvSignUp


/**
 * 注册 [Fragment].
 */
class SignUpFragment : Fragment() {

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater?.inflate(R.layout.fragment_sign_up, container, false)
    }

    var tryLogin: ((Unit) -> Unit)? = null

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        tvSignUp?.setTintDrawable(resources, R.mipmap.ic_chevron_right, R.color.text_primary)
        tvLogin?.setOnClickListener {
            tryLogin?.invoke(Unit)
        }
        tvSignUp?.setOnClickListener {
            requestRegister(inputUsername.textString(),
                    inputPassword.textString(),
                    inputEmail.textString())
        }
    }

    /**
     * 请求注册账号
     */
    private fun requestRegister(username: String, password: String, email: String) {
        val userMap = mapOf(
                "username" to username,
                "password" to password,
                "email" to email
        )
        val api = RetrofitFactory.build(Api.BASE_URL).create(Api::class.java)
        api.register(userMap)
                .compose(RxHttpSchedulers.composeObservable())
                .subscribe({ response: Response<User>? ->
                    if (response == null || !response.success || response.data == null) {
                        toast("注册失败! 请检查信息.")
                    } else {
                        toast("注册成功")
                        prepareLogin(response.data!!)
                        activity?.finish()
                        jump2Activity(MainActivity::class.java)
                    }
                }) {
                    it.httpLog()
                }
    }

    /**
     * 保存用户信息
     */
    private fun prepareLogin(user: User) {
        var username by Preference(activity, Preference.USERNAME, "")
        var nickname by Preference(activity, Preference.NICKNAME, "")
        var avatar by Preference(activity, Preference.AVATAR, "")
        var email by Preference(activity, Preference.EMAIL, "")
        username = user.username
        nickname = user.nickname
        avatar = user.avatar
        email = user.email
    }

}
