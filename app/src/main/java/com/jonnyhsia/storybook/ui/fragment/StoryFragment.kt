package com.jonnyhsia.storybook.ui.fragment

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.PagerSnapHelper
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.jonnyhsia.storybook.R
import com.jonnyhsia.storybook.persistence.Story
import com.jonnyhsia.storybook.ui.adapter.ImagesAdapter
import com.jonnyhsia.storybook.utils.Preference
import com.jonnyhsia.storybook.utils.UIUtils
import kotlinx.android.synthetic.main.fragment_story.tvStoryContent
import kotlinx.android.synthetic.main.fragment_story.tvStoryTitle
import kotlinx.android.synthetic.main.fragment_story.viewStubImgs
import kotlinx.android.synthetic.main.layout_imgs.rvImages
import java.util.Date

/**
 * 显示故事内容
 */
class StoryFragment : Fragment() {

    val story: Story by lazy {
        Story(arguments.getLong(ID),
                arguments.getString(TITLE),
                arguments.getString(CONTENT),
                arguments.getString(AUTHOR),
                arguments.getString(IMGS),
                Date(arguments.getLong(DATE, 0L)))
    }

    private var imgAdapter: ImagesAdapter? = null

    private var textSize = 15

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater?.inflate(R.layout.fragment_story, container, false)
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val size by Preference(activity, Preference.STORY_TEXT_SIZE, 15)
        textSize = size
    }

    override fun onStart() {
        super.onStart()
        tvStoryTitle?.text = story.title
        tvStoryContent?.text = story.content
        tvStoryContent?.textSize = textSize.toFloat()

        // 判断故事是否有照片
        if (story.images.isNotEmpty()) {
            viewStubImgs?.inflate()
            initImages()
        } else {
            // 无照片故事正文上边距相应增大
            tvStoryTitle?.setPadding(0, UIUtils.dp2px(12f, context).toInt(), 0, 0)
        }
    }

    private fun initImages() {
        imgAdapter = ImagesAdapter(story.imagesArray())
        rvImages?.apply {
            setHasFixedSize(true)
            layoutManager = LinearLayoutManager(activity, LinearLayoutManager.HORIZONTAL, false)
            adapter = imgAdapter
        }
        PagerSnapHelper().attachToRecyclerView(rvImages)
    }

    companion object {
        val ID = "id"
        val TITLE = "title"
        val CONTENT = "content"
        val IMGS = "imgs"
        val AUTHOR = "author"
        val DATE = "date"

        fun newInstance(story: Story): StoryFragment {
            val instance = StoryFragment()
            val args = Bundle()

            args.putLong(ID, story.storyId)
            args.putString(TITLE, story.title)
            args.putString(CONTENT, story.content)
            args.putString(AUTHOR, story.author)
            args.putString(IMGS, story.images)
            args.putLong(DATE, story.createTime.time)
            instance.arguments = args

            return instance
        }
    }

    fun notifyStoryChanged(title: String?, content: String?, imgs: String?) {
        story.title = title ?: ""
        story.content = content ?: ""
        // TODO 图片显示的更新
        // imgAdapter?.updateData(imgs?.split(";") as ArrayList<String>)
    }

    fun notifyStoryChanged(story: Story) {
        notifyStoryChanged(story.title, story.content, story.images)
    }
}