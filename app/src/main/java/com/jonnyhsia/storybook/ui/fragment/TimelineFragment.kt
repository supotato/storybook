package com.jonnyhsia.storybook.ui.fragment

import android.arch.lifecycle.ViewModelProviders
import android.content.Context
import android.content.Intent
import android.graphics.Canvas
import android.os.Bundle
import android.support.design.widget.BaseTransientBottomBar
import android.support.design.widget.Snackbar
import android.support.v4.app.ActivityOptionsCompat
import android.support.v4.app.Fragment
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.helper.ItemTouchHelper
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.jonnyhsia.storybook.App

import com.jonnyhsia.storybook.R
import com.jonnyhsia.storybook.exception.HttpRequestFailedException
import com.jonnyhsia.storybook.persistence.Story
import com.jonnyhsia.storybook.http.Api
import com.jonnyhsia.storybook.http.RetrofitFactory
import com.jonnyhsia.storybook.persistence.StorybookDatabase
import com.jonnyhsia.storybook.rx.HttpSingleObserver
import com.jonnyhsia.storybook.rx.RxHttpSchedulers
import com.jonnyhsia.storybook.ui.OnItemClickListener
import com.jonnyhsia.storybook.ui.Scroll2Top
import com.jonnyhsia.storybook.ui.activity.AccountActivity
import com.jonnyhsia.storybook.ui.activity.StoryDetailActivity
import com.jonnyhsia.storybook.ui.activity.WelcomeActivity
import com.jonnyhsia.storybook.ui.adapter.TimelineAdapter
import com.jonnyhsia.storybook.ui.viewholder.TimelineHolder
import com.jonnyhsia.storybook.ui.viewmodel.StoriesViewModel
import com.jonnyhsia.storybook.ui.viewmodel.ViewModelFactory
import com.jonnyhsia.storybook.utils.Preference
import com.jonnyhsia.storybook.utils.UIUtils
import com.jonnyhsia.storybook.utils.jump2Activity
import com.jonnyhsia.storybook.utils.loge
import com.jonnyhsia.storybook.utils.toast
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.fragment_timeline.stickyView
import kotlinx.android.synthetic.main.fragment_timeline.nestedScrollView
import kotlinx.android.synthetic.main.fragment_timeline.stickyLayout
import kotlinx.android.synthetic.main.fragment_timeline.topView
import java.text.SimpleDateFormat
import java.util.Date
import java.util.Locale
import kotlin.properties.Delegates
import android.support.v4.content.ContextCompat
import android.support.v4.util.Pair
import android.text.InputType
import com.jonnyhsia.storybook.ui.activity.SearchActivity
import com.jonnyhsia.storybook.utils.SnackbarCompat

/**
 * 首页时间线 [Fragment]
 */
class TimelineFragment : Fragment(), Scroll2Top {

    /**
     * Story View Model
     */
    private lateinit var viewModel: StoriesViewModel

    /**
     * View Model Factory
     */
    private lateinit var viewModelFactory: ViewModelFactory

    /**
     * RecyclerView Data
     */
    private var timelineData by Delegates.observable(ArrayList<Story>(), { _, oldValue, newValue ->
        if (oldValue != newValue) {
            adapter?.updateData(newValue)
        }
    })

    /**
     * 适配器
     */
    private var adapter: TimelineAdapter? = null

    /**
     * 管理 Rx 事件的订阅 (?)
     */
    private val disposable = CompositeDisposable()

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        // 初始化 ViewModel 与 Factory
        if (context != null) {
            viewModelFactory = ViewModelFactory(StorybookDatabase.instance(context).storyDao())
            viewModel = ViewModelProviders.of(this, viewModelFactory).get(StoriesViewModel::class.java)
        }
    }

    override fun onStart() {
        super.onStart()
        disposable.clear()
        disposable.add(viewModel.stories()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ stories ->
                    // 成功查询到故事
                    timelineData = stories as ArrayList<Story>
                }, { e ->
                    loge(e.message, e)
                })
        )
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater?.inflate(R.layout.fragment_timeline, container, false)
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        topView.mButtonClick = {
            jump2Activity(AccountActivity::class.java)
        }
        val drawable = stickyView?.compoundDrawablesRelative?.get(0)
        if (drawable != null) {
            UIUtils.tintDrawable(drawable = drawable, colors = resources.getColor(R.color.text_primary))
        }
        initView()
        requestTimeline()
    }

    /**
     * 获取时间线数据
     */
    private fun requestTimeline() {
        val username by Preference(activity, Preference.USERNAME, "")
        // 简单判断用户名是否合法
        if (username.isEmpty()) {
            activity.finish()
            val intent = Intent(activity, WelcomeActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
        }
        // 请求接口
        val api = RetrofitFactory.build(Api.BASE_URL).create(Api::class.java)
        api.getTimelineStory(username)
                .compose(RxHttpSchedulers.composeSingle())
                .subscribe(HttpSingleObserver({ data ->
                    data?.let { updateLocalStory(it) }
                }, {
                    toast("$it")
                }))
    }

    /**
     * 更新本地数据库
     * 先清空本地故事, 再插入数据
     */
    private fun updateLocalStory(data: ArrayList<Story>) {
        disposable.add(viewModel.clearStories()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnComplete {
                    // 完成本地故事的清除
                }
                .observeOn(Schedulers.io())
                .andThen(viewModel.insertStories(data))
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    // 完成本地故事的写入
                }, {
                    loge(it.message)
                })
        )
    }

    private fun initView() {
        topView?.subTitle = SimpleDateFormat("MM月dd日, yyyy", Locale.CHINA).format(Date())
        stickyView?.typeface = App.TYPEFACE
        stickyView?.setOnClickListener {
            val intent = Intent(activity, SearchActivity::class.java)
            val pair = Pair.create(stickyView as View, stickyView.transitionName)
            startActivity(intent, ActivityOptionsCompat.makeSceneTransitionAnimation(
                    activity, pair).toBundle());
        }
        stickyView?.showSoftInputOnFocus = false
        stickyView?.inputType = InputType.TYPE_NULL
        nestedScrollView?.setHasFixedSize(true)
        nestedScrollView?.layoutManager = LinearLayoutManager(context)
        nestedScrollView?.itemAnimator = DefaultItemAnimator()

        adapter = TimelineAdapter(timelineData)
        nestedScrollView?.adapter = adapter
        nestedScrollView?.itemAnimator = DefaultItemAnimator()
        nestedScrollView?.addOnItemTouchListener(OnItemClickListener(nestedScrollView, { _, holder ->
            if (holder is TimelineHolder) {
                startActivity(StoryDetailActivity.getStoryIntent(activity, timelineData[holder.adapterPosition].storyId))
            }
        }))

        touchHelper.attachToRecyclerView(nestedScrollView)
    }

    private val touchHelper = ItemTouchHelper(object : ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.END) {
        override fun onMove(recyclerView: RecyclerView?,
                            viewHolder: RecyclerView.ViewHolder?,
                            target: RecyclerView.ViewHolder?) = false

        override fun onChildDraw(c: Canvas,
                                 recyclerView: RecyclerView,
                                 viewHolder: RecyclerView.ViewHolder,
                                 dX: Float, dY: Float,
                                 actionState: Int,
                                 isCurrentlyActive: Boolean) {
            if (actionState == ItemTouchHelper.ACTION_STATE_SWIPE && isCurrentlyActive) {
                val itemView = viewHolder.itemView
                val d = ContextCompat.getDrawable(context, R.drawable.swipe_drawable)
                d.setBounds(itemView.left, itemView.top, dX.toInt(), itemView.bottom)
                d.draw(c)
            }
            super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive)
        }

        override fun onSwiped(viewHolder: RecyclerView.ViewHolder?, direction: Int) {
            when (direction) {
                ItemTouchHelper.END -> {
                    val pos = viewHolder?.adapterPosition?.takeIf { it != -1 } ?: return
                    val story = adapter?.adapterData?.get(pos) ?: return

                    // 将故事添加到删除队列
                    // 删除 RecyclerView 中对应的故事
                    deleteQueue.add(0, story)
                    adapter?.adapterData?.removeAt(pos)
                    adapter?.notifyItemRemoved(pos)

                    SnackbarCompat.make(view = viewHolder.itemView, text = "删除一篇故事 \"${story.title}\"", duration = Snackbar.LENGTH_LONG, additionMargin = true)
                            .setActionTextColor(context.resources.getColor(R.color.highlight))
                            .setAction("撤销", {
                                // 撤销删除
                                deleteQueue.remove(story)
                                adapter?.adapterData?.add(pos, story)
                                adapter?.notifyItemInserted(pos)
                            })
                            .addCallback(object : BaseTransientBottomBar.BaseCallback<Snackbar>() {
                                override fun onDismissed(transientBottomBar: Snackbar?, event: Int) {
                                    super.onDismissed(transientBottomBar, event)
                                    // 判断 event 类型
                                    // 非 Action 引起的 Dismiss 则进行删除操作
                                    if (event == BaseTransientBottomBar.BaseCallback.DISMISS_EVENT_ACTION) return
                                    deleteQueue.remove(story)
                                    requestDeleteStory(story)
                                }
                            })
                            .show()
                }
            }
        }
    })

    val deleteQueue = ArrayList<Story>()

    private fun requestDeleteStory(story: Story) {
        val api = RetrofitFactory.build(Api.BASE_URL).create(Api::class.java)
        api.deleteStory(story.storyId)
                .compose(RxHttpSchedulers.composeSingle())
                .observeOn(Schedulers.io())
                .flatMapCompletable {
                    // 删除本地故事
                    if (it.success) viewModel.deleteStory(story)
                    else throw HttpRequestFailedException("删除故事疑似失败?")
                }
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    // 删除列表故事数据
                    toast("删除故事成功。")
                }, {
                    loge("本地故事删除失败", it)
                    toast("本地故事删除失败, 请稍后再重试。")
                })
    }

    override fun scroll2Top() {
        nestedScrollView?.smoothScrollToPosition(0)
        stickyLayout?.scrollTo(0, 0)
    }

    override fun onDestroy() {
        super.onDestroy()
        disposable.clear()
    }
}
