package com.jonnyhsia.storybook.ui.glide

import android.graphics.Bitmap
import android.graphics.BitmapShader
import android.graphics.Canvas
import android.graphics.Paint
import android.graphics.Shader

import com.bumptech.glide.load.engine.bitmap_recycle.BitmapPool
import com.bumptech.glide.load.resource.bitmap.BitmapTransformation

import java.security.MessageDigest

/**
 * Created by JonnyHsia on 17/8/8.
 */
class CircleTransform : BitmapTransformation() {
    override fun transform(pool: BitmapPool, toTransform: Bitmap, outWidth: Int, outHeight: Int): Bitmap {
        return circleCrop(pool, toTransform)
    }

    private fun circleCrop(pool: BitmapPool, source: Bitmap?): Bitmap {
        if (source == null) {
            throw IllegalArgumentException("SourceBitmap cannot be null.")
        }

        val size = Math.min(source.width, source.height)
        val x = (source.width - size) / 2
        val y = (source.height - size) / 2

        val squared = Bitmap.createBitmap(source, x, y, size, size)

        val result = pool.get(size, size, Bitmap.Config.ARGB_8888)

        val canvas = Canvas(result)
        val paint = Paint()
        // 画布中背景图片与绘制图片交集部分
        paint.shader = BitmapShader(squared, Shader.TileMode.CLAMP, Shader.TileMode.CLAMP)
        paint.isAntiAlias = true
        val r = size / 2f
        canvas.drawCircle(r, r, r, paint)
        return result
    }

    override fun updateDiskCacheKey(messageDigest: MessageDigest) {

    }
}
