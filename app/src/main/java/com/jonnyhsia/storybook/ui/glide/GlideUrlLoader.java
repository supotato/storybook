package com.jonnyhsia.storybook.ui.glide;

import android.support.annotation.Nullable;

import com.bumptech.glide.load.Options;
import com.bumptech.glide.load.model.GlideUrl;
import com.bumptech.glide.load.model.ModelCache;
import com.bumptech.glide.load.model.ModelLoader;
import com.bumptech.glide.load.model.ModelLoaderFactory;
import com.bumptech.glide.load.model.MultiModelLoaderFactory;
import com.bumptech.glide.load.model.stream.BaseGlideUrlLoader;

import java.io.InputStream;

/**
 * Created by JonnyHsia on 17/8/8.
 */

class GlideUrlLoader extends BaseGlideUrlLoader<String> {

    private static final ModelCache<String, GlideUrl> urlCache =
            new ModelCache<>(150);

    /**
     * Url的匹配规则
     */
    GlideUrlLoader(ModelLoader<GlideUrl, InputStream> concreteLoader, @Nullable ModelCache<String, GlideUrl> modelCache) {
        super(concreteLoader, modelCache);
    }

    /**
     * If the URL contains a special variable width indicator (eg "__w-200-400-800__")
     * we get the buckets from the URL (200, 400 and 800 in the example) and replace
     * the URL with the best bucket for the requested width (the bucket immediately
     * larger than the requested width).
     * <p>
     * 控制加载的图片的大小
     */
    @Override
    protected String getUrl(String s, int width, int height, Options options) {
        return null;
    }

    @Override
    public boolean handles(String s) {
        return true;
    }

    /**
     * 工厂来构建 BaseGlideUrlLoader 对象
     */
    public static class Factory implements ModelLoaderFactory<String, InputStream> {
        @Override
        public ModelLoader<String, InputStream> build(MultiModelLoaderFactory multiFactory) {
            return new GlideUrlLoader(multiFactory.build(GlideUrl.class, InputStream.class), urlCache);
        }

        @Override
        public void teardown() {

        }
    }
}
