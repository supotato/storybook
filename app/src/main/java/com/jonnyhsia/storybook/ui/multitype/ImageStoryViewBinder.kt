package com.jonnyhsia.storybook.ui.multitype

import android.graphics.Color
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import com.bumptech.glide.Glide
import com.jonnyhsia.storybook.R
import com.jonnyhsia.storybook.persistence.Story
import com.jonnyhsia.storybook.utils.UIUtils
import com.jonnyhsia.storybook.utils.find
import com.makeramen.roundedimageview.RoundedImageView
import me.drakeet.multitype.ItemViewBinder

/**
 * Created by JonnyHsia on 17/9/14.
 * 纯文字故事 Item View Binder
 */
class ImageStoryViewBinder(onClick: (pos: Int) -> Unit)
    : TextStoryViewBinder(onClick) {

    override fun onCreateViewHolder(inflater: LayoutInflater, parent: ViewGroup): ViewHolder {
        val root = inflater.inflate(R.layout.item_timeline, parent, false)
        return ViewHolder(root)
    }

    override fun onBindViewHolder(holder: TextStoryViewBinder.ViewHolder, story: Story) {
        super.onBindViewHolder(holder, story)
        holder.itemView?.setOnClickListener {
            onClick(holder.adapterPosition)
        }
        // 判断 story 是否为 null 或 empty
        if (story.imagesArray().isNotEmpty()) {
            val imgs = story.imagesArray()
            val context = holder.itemView.context
            val maxSize = 6
            val glide = Glide.with(context)

            // 获取单位 dp 折合的 px 值
            val dip = UIUtils.dp2px(1f, context)
            // 移除 imgsContainer 中所有的 ChildViews
            holder.imgsContainer?.removeAllViews()

            // 遍历, 依次添加 ImageView
            for (i in 0 until minOf(imgs.size, maxSize)) {
                // 通过 ImageView 构造方法创建一个 ImageView 的实例
                val imageView = RoundedImageView(context)

                // 给 ImageView 设置布局参数 (大小, 边距等)
                val params = LinearLayout.LayoutParams((72 * dip).toInt(), (72 * dip).toInt())
                // 设置 ImageView 的边距 (5dp)
                val margin = (5 * dip).toInt()
                params.setMargins(margin, margin, margin, margin)
                imageView.layoutParams = params

                // 设置 ImageView 的属性
                imageView.scaleType = ImageView.ScaleType.CENTER_CROP
                imageView.cornerRadius = 6 * dip
                imageView.borderColor = Color.parseColor("#1f000000")
                imageView.borderWidth = dip

                // 加载图片
                glide.load(imgs[i])
                        .into(imageView)
                // 添加到 imgsContainer 中
                // 由于 imgsContainer 是 LinearLayout 会自动完成 ImageView 的排列布局
                holder.imgsContainer?.addView(imageView)
            }
        }
    }
}