package com.jonnyhsia.storybook.ui.multitype

import android.annotation.SuppressLint
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView

import com.jonnyhsia.storybook.R
import com.jonnyhsia.storybook.utils.find

import me.drakeet.multitype.ItemViewBinder
import android.net.Uri
import android.support.customtabs.CustomTabsIntent

/**
 * Created by JonnyHsia on 17/9/17.
 * 第三方库列表 Binder
 */
class LibraryViewBinder : ItemViewBinder<String, LibraryViewBinder.ViewHolder>() {
    private var builder: CustomTabsIntent.Builder? = null
    private var customTabsIntent: CustomTabsIntent? = null

    init {
        builder = CustomTabsIntent.Builder()
        customTabsIntent = builder?.build()
    }

    override fun onCreateViewHolder(inflater: LayoutInflater, parent: ViewGroup): ViewHolder {
        val root = inflater.inflate(R.layout.item_library, parent, false)
        return ViewHolder(root)
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: ViewHolder, library: String) {
        val pos = holder.adapterPosition
        // 根据 position 判断 item view 的背景色
        val bgColor = holder.itemView.resources.getColor(
                if (pos % 2 == 0) R.color.window_bg else R.color.lib_bg_different)
        holder.itemView.setBackgroundColor(bgColor)
        // 分割 library 字符串
        val libInfo = library.split("#")
        holder.tvName?.text = "${libInfo[0]} - ${libInfo[1]}"
        holder.itemView.setOnClickListener {
            builder?.setToolbarColor(bgColor)
            customTabsIntent?.launchUrl(holder.itemView.context, Uri.parse(libInfo[2]))
        }
        holder.tvDescription?.text = libInfo[3]
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val tvName: TextView? = find(R.id.tvLibName)
        val tvDescription: TextView? = find(R.id.tvLibDescription)
    }
}