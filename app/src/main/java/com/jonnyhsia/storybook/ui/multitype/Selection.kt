package com.jonnyhsia.storybook.ui.multitype

/**
 * Created by JonnyHsia on 17/9/14.
 */
data class Selection(var title: String,
                     var action: String? = null,
                     var url: String? = null)