package com.jonnyhsia.storybook.ui.multitype

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView

import com.jonnyhsia.storybook.R
import com.jonnyhsia.storybook.utils.find
import com.jonnyhsia.storybook.utils.toast

import me.drakeet.multitype.ItemViewBinder

/**
 * Created by JonnyHsia on 17/9/14.
 * Section Header
 */
class SelectionViewBinder : ItemViewBinder<Selection, SelectionViewBinder.ViewHolder>() {

    override fun onCreateViewHolder(inflater: LayoutInflater, parent: ViewGroup): ViewHolder {
        val root = inflater.inflate(R.layout.item_selection, parent, false)
        return ViewHolder(root)
    }

    override fun onBindViewHolder(holder: ViewHolder, selection: Selection) {
        holder.tvTitle?.text = selection.title
        holder.tvAction?.apply {
            text = selection.action
            setOnClickListener {
                context.toast("$selection")
            }
        }
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var tvTitle: TextView? = find(R.id.tvSection)
        var tvAction: TextView? = find(R.id.tvAction)
    }
}