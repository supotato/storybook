package com.jonnyhsia.storybook.ui.multitype

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.jonnyhsia.storybook.GlideApp

import com.jonnyhsia.storybook.R
import com.jonnyhsia.storybook.persistence.Subscription
import com.jonnyhsia.storybook.utils.find

import me.drakeet.multitype.ItemViewBinder

/**
 * Created by JonnyHsia on 17/9/14.
 * 订阅
 */
class SubscriptionViewBinder : ItemViewBinder<Subscription, SubscriptionViewBinder.ViewHolder>() {

    override fun onCreateViewHolder(inflater: LayoutInflater, parent: ViewGroup): ViewHolder {
        val root = inflater.inflate(R.layout.item_subscription, parent, false)
        return ViewHolder(root)
    }

    override fun onBindViewHolder(holder: ViewHolder, subscription: Subscription) {
        holder.tvTitle?.text = subscription.title
        GlideApp.with(holder.itemView)
                .load(subscription.img)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(holder.imageView)
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var tvTitle: TextView? = find(R.id.tvSubscriptionTitle)
        var imageView: ImageView? = find(R.id.imgSubscription)
    }
}
