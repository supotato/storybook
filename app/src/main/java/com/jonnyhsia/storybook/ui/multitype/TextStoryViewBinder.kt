package com.jonnyhsia.storybook.ui.multitype

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.jonnyhsia.storybook.R
import com.jonnyhsia.storybook.persistence.Story
import com.jonnyhsia.storybook.utils.find
import com.jonnyhsia.storybook.utils.toast
import me.drakeet.multitype.ItemViewBinder

/**
 * Created by JonnyHsia on 17/9/14.
 * 纯文字故事 Item View Binder
 */
open class TextStoryViewBinder(var onClick: (pos: Int) -> Unit)
    : ItemViewBinder<Story, TextStoryViewBinder.ViewHolder>() {

    override fun onCreateViewHolder(inflater: LayoutInflater, parent: ViewGroup): ViewHolder {
        val root = inflater.inflate(R.layout.item_timeline, parent, false)
        return ViewHolder(root)
    }

    override fun onBindViewHolder(holder: ViewHolder, story: Story) {
        holder.tvTitle?.text = story.title
        holder.tvContent?.text = story.content
        holder.itemView?.setOnClickListener {
            onClick(holder.adapterPosition)
        }
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var tvTitle: TextView? = find(R.id.tvTitle)
        var tvContent: TextView? = find(R.id.tvContent)
        val imgsContainer = find<ViewGroup>(R.id.imgsContainer)
    }
}