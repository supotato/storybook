package com.jonnyhsia.storybook.ui.view


import android.content.Context
import android.support.v7.widget.AppCompatTextView
import android.util.AttributeSet
import com.jonnyhsia.storybook.App

/**
 * Created by JonnyHsia on 17/8/4.
 * 去除字体 Padding 的 [AppCompatTextView]
 */
class NotoTextView : AppCompatTextView {

    constructor(context: Context) : super(context) {
        init(context, null)
    }

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        init(context, attrs)
    }


    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        init(context, attrs)
    }

    private fun init(context: Context, attrs: AttributeSet?) {
        includeFontPadding = false
        typeface = App.TYPEFACE
    }

}
