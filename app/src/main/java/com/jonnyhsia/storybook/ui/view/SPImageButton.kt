package com.jonnyhsia.storybook.ui.view

import android.content.Context
import android.support.v7.widget.AppCompatImageButton
import android.util.AttributeSet

/**
 * Created by JonnyHsia on 17/8/26.
 */
class SPImageButton : AppCompatImageButton {

    constructor(context: Context) : super(context) {
        init(context, null)
    }

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        init(context, attrs)
    }

    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        init(context, attrs)
    }

    private fun init(context: Context, attrs: AttributeSet?) {
    }
}
