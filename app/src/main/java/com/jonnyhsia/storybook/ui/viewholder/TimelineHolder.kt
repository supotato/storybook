package com.jonnyhsia.storybook.ui.viewholder

import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.TextView
import com.jonnyhsia.storybook.App
import com.jonnyhsia.storybook.R
import com.jonnyhsia.storybook.persistence.Story
import com.jonnyhsia.storybook.utils.Utils
import com.jonnyhsia.storybook.utils.find

/**
 * Created by JonnyHsia on 17/8/4.
 * 纯文字 Timeline ViewHolder
 */
open class TimelineHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    val tvTitle = find<TextView>(R.id.tvTitle)
    val tvContent = find<TextView>(R.id.tvContent)

    /**
     * Bind data to view holder
     */
    open fun bind(data: Story) {
//        tvTitle?.text = data.title
        tvContent?.text = Utils.deleteBlankLine(data.content)
        // 给 TextView 设置字体
        tvContent?.typeface = App.TYPEFACE
    }
}