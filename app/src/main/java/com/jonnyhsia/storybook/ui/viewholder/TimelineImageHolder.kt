package com.jonnyhsia.storybook.ui.viewholder

import android.graphics.Color
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import com.bumptech.glide.Glide
import com.jonnyhsia.storybook.R
import com.jonnyhsia.storybook.persistence.Story
import com.jonnyhsia.storybook.utils.UIUtils
import com.jonnyhsia.storybook.utils.find
import com.makeramen.roundedimageview.RoundedImageView

/**
 * Created by JonnyHsia on 17/8/4.
 * 含图片的 ViewHolder
 */
class TimelineImageHolder(itemView: View)
    : TimelineHolder(itemView) {

    private val imgsContainer = find<ViewGroup>(R.id.imgsContainer)

    // 最多一次只显示六张图片
    private var maxSize: Int = 6
        set(value) {
            if (value in 1..9) {
                field = value
            }
        }

    override fun bind(data: Story) {
        super.bind(data)
        // 判断 data 是否为 null 或 empty
        if (data.imagesArray().isNotEmpty()) {
            val imgs = data.imagesArray()
            val context = itemView.context
            val glide = Glide.with(context)

            // 获取单位 dp 折合的 px 值
            val dip = UIUtils.dp2px(1f, context)
            // 移除 imgsContainer 中所有的 ChildViews
            imgsContainer?.removeAllViews()

            // 遍历, 依次添加 ImageView
            for (i in 0 until minOf(imgs.size, maxSize)) {
                // 通过 ImageView 构造方法创建一个 ImageView 的实例
                val imageView = RoundedImageView(context)

                // 给 ImageView 设置布局参数 (大小, 边距等)
                val params = LinearLayout.LayoutParams((72 * dip).toInt(), (72* dip).toInt())
                // 设置 ImageView 的边距 (5dp)
                val margin = (5 * dip).toInt()
                params.setMargins(margin, margin, margin, margin)
                imageView.layoutParams = params

                // 设置 ImageView 的属性
                imageView.scaleType = ImageView.ScaleType.CENTER_CROP
                imageView.cornerRadius = 6 * dip
                imageView.borderColor = Color.parseColor("#1f000000")
                imageView.borderWidth = dip

                // 加载图片
                glide.load(imgs[i])
                        .into(imageView)
                // 添加到 imgsContainer 中
                // 由于 imgsContainer 是 LinearLayout 会自动完成 ImageView 的排列布局
                imgsContainer?.addView(imageView)
            }
        }
    }

}