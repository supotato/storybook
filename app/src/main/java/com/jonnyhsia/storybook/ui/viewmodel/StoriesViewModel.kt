package com.jonnyhsia.storybook.ui.viewmodel

import android.arch.lifecycle.ViewModel
import com.jonnyhsia.storybook.persistence.Story
import com.jonnyhsia.storybook.persistence.StoryDao
import io.reactivex.Completable
import io.reactivex.Flowable
import io.reactivex.functions.Action
import io.reactivex.internal.operators.completable.CompletableFromAction

/**
 * Created by JonnyHsia on 17/8/30.
 * 故事 [ViewModel]
 */
class StoriesViewModel(private val storyDao: StoryDao) : ViewModel() {

    /**
     * 获取所有故事
     * @return 返回 [Flowable] 每次故事列表数据更新都会发射一次事件
     */
    fun stories(): Flowable<List<Story>> {
        return storyDao.queryAllStory()
    }

    /**
     * 删除指定的故事
     */
    fun deleteStory(story: Story): Completable {
        return CompletableFromAction(Action {
            storyDao.deleteStory(story)
        })
    }

    /**
     * 清除所有故事
     */
    fun clearStories(): Completable {
        return CompletableFromAction(Action {
            storyDao.clearStories()
        })
    }

    /**
     * 插入故事数据
     * @param stories 新的故事数据
     * @return 返回 [Completable] 对象, 当故事成功插入后会调用"完成"
     */
    fun insertStories(stories: ArrayList<Story>): Completable {
        return CompletableFromAction(Action {
            storyDao.insertStories(stories)
        })
    }
}