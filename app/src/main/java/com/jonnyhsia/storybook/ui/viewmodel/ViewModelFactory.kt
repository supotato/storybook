package com.jonnyhsia.storybook.ui.viewmodel

import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import com.jonnyhsia.storybook.persistence.StoryDao

/**
 * Factory for ViewModels
 */
class ViewModelFactory(private val dataSource: StoryDao) : ViewModelProvider.Factory {

    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        when {
            modelClass.isAssignableFrom(StoriesViewModel::class.java) -> return StoriesViewModel(dataSource) as T
            else -> throw IllegalArgumentException("Unknown ViewModel class")
        }
    }
}