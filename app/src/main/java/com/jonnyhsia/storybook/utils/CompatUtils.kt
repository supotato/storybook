package com.jonnyhsia.storybook.utils

import android.os.Build

/**
 * Created by JonnyHsia on 17/9/6.
 * 兼容支持
 */
object CompatUtils {

    /**
     * 根据 [Build.VERSION.SDK_INT] 执行 Java8 代码, 或兼容代码
     * @param j8Code Java8 代码
     * @param compatCode 兼容代码
     */
    inline fun supportJ8(j8Code: () -> Unit, compatCode: () -> Unit) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            j8Code()
        } else {
            compatCode()
        }
    }
}