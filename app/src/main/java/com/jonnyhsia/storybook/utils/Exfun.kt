package com.jonnyhsia.storybook.utils

import android.content.Context
import android.content.Intent
import android.content.res.Resources
import android.graphics.drawable.Drawable
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.Gravity
import android.view.View
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import retrofit2.HttpException

/**
 * Created by JonnyHsia on 17/8/2.
 * 插件方法
 */
fun Any.logd(msg: String?, e: Throwable? = null): Unit {
    Log.d(javaClass.simpleName, msg, e)
}

fun Any.loge(msg: String?, e: Throwable? = null): Unit {
    Log.e(javaClass.simpleName, msg, e)
}

fun <T : View> RecyclerView.ViewHolder.find(id: Int): T? {
    return itemView.findViewById(id)
}

fun Context.toast(msg: String?) {
    // Toast.makeText(this, msg, Toast.LENGTH_SHORT).show()
    ToastUtils.showToast(this, msg)
}

fun Fragment.toast(msg: String?) {
    // activity?.toast(msg)
    ToastUtils.showToast(activity, msg)
}

fun View.toast(msg: String?) {
    // activity?.toast(msg)
    ToastUtils.showToast(context, msg)
}

fun EditText.textString(): String {
    return this.text.toString()
}

fun Fragment.jump2Activity(cls: Class<*>, requestCode: Int? = null): Unit {
    if (requestCode == null) {
        startActivity(Intent(activity, cls))
    } else {
        startActivityForResult(Intent(activity, cls), requestCode)
    }
}

fun AppCompatActivity.jump2Activity(cls: Class<*>, requestCode: Int? = null): Unit {
    if (requestCode == null) {
        startActivity(Intent(this, cls))
    } else {
        startActivityForResult(Intent(this, cls), requestCode)
    }
}

fun TextView.setTintDrawable(drawable: Drawable, tint: Int, gravity: Int = Gravity.END): Unit {
    drawable.setBounds(0, 0, drawable.minimumWidth, drawable.minimumHeight)
    drawable.setTint(tint)
    when (gravity) {
        Gravity.END -> setCompoundDrawables(null, null, drawable, null)
    }
}

fun TextView.setTintDrawable(resources: Resources, drawableRes: Int, colorRes: Int, gravity: Int = Gravity.END): Unit {
    setTintDrawable(resources.getDrawable(drawableRes), resources.getColor(colorRes), gravity)
}

fun Throwable.httpLog(): Unit {
    if (this is HttpException) {
        loge(this.response().errorBody()?.string().toString())
    } else {
        printStackTrace()
    }
}