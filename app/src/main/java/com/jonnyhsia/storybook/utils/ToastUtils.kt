package com.jonnyhsia.storybook.utils

import android.annotation.SuppressLint
import android.content.Context
import android.widget.Toast

/**
 * Created by JonnyHsia on 17/9/16.
 */
object ToastUtils {
    var toast: Toast? = null

    @SuppressLint("ShowToast")
    fun showToast(context: Context?, message: String?, duration: Int = Toast.LENGTH_SHORT) {
        if (message == null || message.isEmpty())
            return

        if (toast == null) {
            if (context == null)
                return
            toast = Toast.makeText(context, message, duration)
        } else {
            toast?.setText(message)
        }
        toast?.show()
    }
}