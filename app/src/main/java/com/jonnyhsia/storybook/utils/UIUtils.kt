package com.jonnyhsia.storybook.utils

import android.content.Context
import android.graphics.drawable.Drawable
import android.support.v4.graphics.drawable.DrawableCompat
import android.util.DisplayMetrics
import android.util.TypedValue
import android.view.Window
import android.view.WindowManager

/**
 * Created by JonnyHsia on 17/8/1.
 * UI 工具类
 */
object UIUtils {

    /**
     * dp 转换为 px
     */
    fun dp2px(dp: Float, context: Context): Float {
        return dp2px(dp, context.resources.displayMetrics)
    }

    private fun dp2px(dp: Float, metrics: DisplayMetrics): Float {
        return TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, metrics)
    }

    fun px2sp(context: Context, pxValue: Float): Float {
        return pxValue / (context.resources.displayMetrics.scaledDensity)
    }

    /**
     * 设置状态栏图标为深色
     * @param window 需要设置的窗口
     * @param dark   是否把状态栏字体及图标颜色设置为深色
     * @return boolean 成功执行返回true
     */
    fun statusBarLightMode(window: Window?, dark: Boolean): Boolean {
        var result = false
        if (window != null) {
            try {
                val lp = window.attributes
                val darkFlag = WindowManager.LayoutParams::class.java
                        .getDeclaredField("MEIZU_FLAG_DARK_STATUS_BAR_ICON")
                val meizuFlags = WindowManager.LayoutParams::class.java
                        .getDeclaredField("meizuFlags")
                darkFlag.isAccessible = true
                meizuFlags.isAccessible = true
                val bit = darkFlag.getInt(null)
                var value = meizuFlags.getInt(lp)
                value = if (dark) {
                    value or bit
                } else {
                    value and bit.inv()
                }
                meizuFlags.setInt(lp, value)
                window.attributes = lp
                result = true
            } catch (e: Exception) {
                // e.printStackTrace()
            }
        }
        return result
    }

    fun tintDrawable(drawable: Drawable, colors: Int): Drawable {
        val wrappedDrawable = DrawableCompat.wrap(drawable).mutate()
        DrawableCompat.setTint(wrappedDrawable, colors)
        return wrappedDrawable
    }

}