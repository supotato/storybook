package com.jonnyhsia.storybook.utils

import android.content.Context
import android.content.pm.PackageInfo
import android.content.pm.PackageManager
import android.content.res.Resources
import android.net.ConnectivityManager
import com.jonnyhsia.storybook.App
import com.jonnyhsia.storybook.R
import java.text.SimpleDateFormat
import java.util.*

/**
 * Created by JonnyHsia on 17/8/9.
 * 工具
 */
object Utils {

    /**
     * 判断网路是否可用
     */
    fun isNetworkWorking(): Boolean {
        val connectivity = App.INSTANCE
                .getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val activeNetwork = connectivity.activeNetworkInfo
        return activeNetwork != null
    }

    /**
     * 获取应用版本号
     */
    fun getVersionName(context: Context): String {
        return getPackageInfo(context)?.versionName.toString()
    }

    private fun getPackageInfo(context: Context): PackageInfo? {
        var info: PackageInfo? = null
        try {
            info = context.packageManager
                    .getPackageInfo(context.packageName, PackageManager.GET_CONFIGURATIONS)
        } catch (e: Exception) {
            e.printStackTrace()
        }
        return info
    }

    fun size2String(size: Int, context: Context): String {
        return size2String(size, context.resources)
    }

    private fun size2String(size: Int, resources: Resources): String {
        return resources.getString(when (size) {
            12 -> R.string.text_size_smallest
            13 -> R.string.text_size_smaller
            14 -> R.string.text_size_smaller_slightly
            15 -> R.string.text_size_smaller_default
            16 -> R.string.text_size_bigger_slightly
            17 -> R.string.text_size_bigger
            18 -> R.string.text_size_biggest
            else -> throw IllegalArgumentException("TextSize is Valid!")
        })
    }

    fun date2tString(date: Date?): String {
        if (date == null) {
            return ""
        }
        val simpleDateFormat = SimpleDateFormat("yy/M/d", Locale.CHINA)
        return simpleDateFormat.format(date)
    }

    fun string2Date(dateString: String?): Date {
        if (dateString == null || dateString.isEmpty()) {
            return Date()
        }
        val simpleDateFormat = SimpleDateFormat("yy/M/d", Locale.CHINA)
        return simpleDateFormat.parse(dateString)
    }

    /***
     * 删除空行与只含空格的行
     * @return
     */
    fun deleteBlankLine(input: String): String {
        return input.replace("((\r\n)|\n)[\\s\t ]*(\\1)+".toRegex(), "$1");
    }

    /**
     * 去除空格空行
     */
    fun deleteBlankSpace(input: String): String {
        return input.replace("\\s*|\t|\r|\n".toRegex(), "")
    }
}